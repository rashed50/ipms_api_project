﻿using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using System.Threading.Tasks;

namespace IPMSAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductListController : ControllerBase
    {
        private IProductListService _productListService;
        public ProductListController(IProductListService productListService)
        {
            _productListService = productListService;
        }

        [HttpGet]
        // GET: ProductListController
        public async Task<IActionResult> Get()
        {
            var productLists = await _productListService.GetAllProductList();

            if (productLists != null)
            {
                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = productLists
                };

                return Ok(result);
            }
            else
            {
                var result = new
                {
                    statusCode = 400,
                    success = false,
                    data = "Data Not Found",
                };

                return BadRequest(result);
            }
        }
    }
}
