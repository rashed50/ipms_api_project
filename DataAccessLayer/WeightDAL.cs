﻿using DataAccessLayer.Interfaces;
using DTO;
using Foundation;
using Microsoft.Extensions.Options;
using Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class WeightDAL : BaseDBAccess, IWeightDAL
    {
        public WeightDAL(IOptions<DBConfiguration> config) : base(config)
        {

        }
        public async Task<List<WeightDTO>> GetAllWeight(int? sizeId)
        {
            try
            {
                SqlConnection conn = new SqlConnection(this.GetConnectionString());
                conn.Open();
                var sp = "Weight_Select_SP";
                SqlCommand cmd = new SqlCommand(sp, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@SizeId", sizeId));
                IDataReader reader = await cmd.ExecuteReaderAsync();
                var weights = new List<WeightDTO>();

                if (reader != null)
                {
                    while (reader.Read())
                    {
                        var weight = new WeightDTO
                        {
                            WeightId = Convert.ToInt32(reader["WeightId"]),
                            WeightName = reader["WeightName"].ToString(),
                            WeightIsActive = Convert.ToBoolean(reader["WeightIsActive"]),
                            SizeId = Convert.ToInt32(reader["SizeId"])
                        };

                        weights.Add(weight);
                    }
                }

                return weights;
            }
            catch (Exception exp)
            {
                return null;
            }
        }

        public async Task AddWeight(WeightModel weightModel)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(this.GetConnectionString()))
                {

                    var sp = "Weight_Insert_SP";
                    SqlCommand cmd = new SqlCommand(sp, conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@WeightName", weightModel.WeightName));
                    cmd.Parameters.Add(new SqlParameter("@SizeId", weightModel.SizeId));
                    cmd.Parameters.Add("@LastID", SqlDbType.Int);
                    cmd.Parameters["@LastID"].Direction = ParameterDirection.Output;
                    await conn.OpenAsync();
                    var reader = await cmd.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
