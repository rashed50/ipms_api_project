﻿namespace DTO
{
    public class StockDTO
    {
        public int StockId { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int BrandId { get; set; }
        public string BrandName { get; set; }
        public int SizeId { get; set; }
        public string SizeName { get; set; }
        public int WeightId { get; set; }
        public string WeightName { get; set; }
        public float LastSaleRate { get; set; }
        public float StockQuantity { get; set; }

    }
}
