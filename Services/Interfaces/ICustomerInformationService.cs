﻿using DTO;
using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface ICustomerInformationService
    {
        Task<List<CustomerInformationDTO>> GetAllCustomers(int? customerType);
        Task AddCustomer(CustomerInformationModel model);
    }
}
