﻿using DTO;

namespace Services.Interfaces
{
    public interface ICompanyInformationService
    {
        CompanyInformationDTO GetCompanyProfileInformation();
    }
}
