﻿using DataAccessLayer.Interfaces;
using DTO;
using Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services
{
    public class UpazilaService : IUpazilaService
    {
        private IUpazilaDAL _upazilaDAL;
        public UpazilaService(IUpazilaDAL upazilaDAL)
        {
            _upazilaDAL = upazilaDAL;
        }

        public async Task<List<UpazilaDTO>> GetAllUpazila()
        {
            return await _upazilaDAL.GetAllUpazila();
        }
    }
}
