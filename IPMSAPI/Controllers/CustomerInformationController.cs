﻿using DTO;
using Foundation;
using Microsoft.AspNetCore.Mvc;
using Models;
using Services.Interfaces;
using System;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace IPMSAPI.Controllers
{
    [Route("api/customers")]
    [ApiController]
    public class CustomerInformationController : ControllerBase
    {
        private ICustomerInformationService _customerInformationService;
        public CustomerInformationController(ICustomerInformationService customerInformationService)
        {
            _customerInformationService = customerInformationService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var list = await _customerInformationService.GetAllCustomers(null);

                var response = new Response
                (
                    data: list,
                    statusCode: "200",
                    success: true
                );

                return Ok(response);

            }
            catch
            {
                var response = new Response
                (
                    data: null,
                    statusCode: "400",
                    success: false
                );

                return BadRequest(response);
            }
        }

        // GET api/<CustomerInformationController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<CustomerInformationController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CustomerInformationModel model)
        {
            try
            {
                await _customerInformationService.AddCustomer(model);

                var response = new Response
                (
                    data : null,
                    statusCode: "200",
                    success: false
                );

                return Ok(response);
            }
            catch(Exception ex)
            {
                var response = new Response
                (
                    data: null,
                    statusCode: "400",
                    success: false
                );

                return BadRequest(response + " " + ex.Message);
            }
        }

        // PUT api/<CustomerInformationController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<CustomerInformationController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
