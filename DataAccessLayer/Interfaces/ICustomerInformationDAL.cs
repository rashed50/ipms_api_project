﻿using DTO;
using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    public interface ICustomerInformationDAL
    {
        public Task<List<CustomerInformationDTO>> GetAllCustomer(int? customerType);
        Task AddCustomer(CustomerInformationModel  model);
    }
}
