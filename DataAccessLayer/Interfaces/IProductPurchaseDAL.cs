﻿using DTO;
using Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    public interface IProductPurchaseDAL
    {
        public Task<int> InsertProductPurchase(ProductPurchaseModel productPurchaseModel);
        public Task UpdateProductPurchase(ProductPurchaseModel productPurchaseModel);
        public Task DeleteProductPurchase(int ID);
        public Task<DataTable> GetProductPurchase();
        public Task<ProductPurchaseDTO> GetProductPurchaseByID(int ID);
        public Task<DataTable> GetPurchaseReportByVendorDetailByToAndFromDate(DateTime fromdate, DateTime todate);
        public Task<DataTable> GetPurchaseTransactionSummary(int vendorId, DateTime fromdate, DateTime todate);
        public Task<DataTable> GetPurchaseReportSummary(int vendorId, DateTime fromdate, DateTime todate);
        public Task<DataTable> GetPurchaseSummaryByVendorId(int vendorId);
        public Task<DataTable> GetPurchaseByItemSummary(DateTime fromdate, DateTime todate, int categoryId, int brandId);
    }
}
