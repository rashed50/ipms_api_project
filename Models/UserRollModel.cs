﻿namespace Models
{
    class UserRollModel
    {
        public string RoleName { get; set; }
        public string RoleDesciption { get; set; }
        public bool RoleIsActive { get; set; }
    }
}
