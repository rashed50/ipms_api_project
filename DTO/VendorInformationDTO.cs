﻿using System;

namespace DTO
{
    public class VendorInformationDTO
    {
        public int VendorId { get; set; }
        public string VendorName { get; set; }
        public string ContactNumber { get; set; }
        public string Address { get; set; }
        public string VendorPhoto { get; set; }
        public double Balance { get; set; }
        public int InitialBalance { get; set; }
        public DateTime OpeningDate { get; set; }
        public int VendorAccountId { get; set; }
        public int EntryById { get; set; }
        public string UserName { get; set; }
    }
}
