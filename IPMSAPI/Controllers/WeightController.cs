﻿using Microsoft.AspNetCore.Mvc;
using Models;
using Services.Interfaces;
using System;
using System.Threading.Tasks;

namespace IPMSAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WeightController : ControllerBase
    {
        private IWeightService _weightService;
        public WeightController(IWeightService weightService)
        {
            _weightService = weightService;
        }

        [HttpGet]
        // GET: WeightController
        public async Task<IActionResult> Get(int? sizeId)
        {
            var weights = await _weightService.GetAllWeight(sizeId);

            if (weights != null)
            {
                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = weights
                };

                return Ok(result);
            }
            else
            {
                var result = new
                {
                    statusCode = 400,
                    success = false,
                    data = "Data Not Found",
                };

                return BadRequest(result);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] WeightModel weightModel)
        {
            try
            {
                await _weightService.AddWeight(weightModel);

                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = ""
                };

                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new
                {
                    statusCode = 400,
                    success = false
                };

                return BadRequest(result);
            }
        }
    }
}
