﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class ProductPurchaseModel
    {
        public int ID { get; set; }
        public int TransactionID { get; set; }
        public double TotalPrice { get; set; }
        //public Int32 Discount { get; set; }
        public int CarringBill { get; set; }
        public DateTime PurchaseDate { get; set; }
        public int VendorID { get; set; }
        public int StaffID { get; set; }
        public double LabourCost { get; set; }
        public int PaymentTypeID { get; set; }
        public int BankID { get; set; }
    }
}
