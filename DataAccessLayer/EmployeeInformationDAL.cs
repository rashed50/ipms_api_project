﻿using DataAccessLayer.Interfaces;
using DTO;
using Foundation;
using Microsoft.Extensions.Options;
using Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class EmployeeInformationDAL : BaseDBAccess, IEmployeeInformationDAL
    {
        public EmployeeInformationDAL(IOptions<DBConfiguration> config) : base(config)
        {

        }

        public async Task<List<EmployeeInformationDTO>> GetAllEmployeeInformation()
        {
            try
            {
                SqlConnection conn = new SqlConnection(this.GetConnectionString());
                conn.Open();
                SqlCommand cmd = new SqlCommand("Select * from EmployeeInformation", conn);
                IDataReader reader = await cmd.ExecuteReaderAsync();
                var employeeInformationDTOs = new List<EmployeeInformationDTO>();

                if (reader != null)
                {
                    while (reader.Read())
                    {
                        var employeeInformationDTO = new EmployeeInformationDTO
                        {
                            EmployeId = Convert.ToInt32(reader["EmployeId"]),
                            EmployeeName = reader["EmployeeName"].ToString(),
                            ContactNumber = reader["ContactNumber"].ToString(),
                            FatherName = reader["FatherName"].ToString(),
                            EmpMobileNo = reader["EmpMobileNo"].ToString(),
                            NID = reader["NID"].ToString(),
                            Photo = reader["Photo"].ToString(),
                            JoinDate = Convert.ToDateTime(reader["JoinDate"]),
                            ReviewDate = Convert.ToDateTime(reader["ReviewDate"]),
                            CountryId = Convert.ToInt32(reader["CountryId"]),
                            DivisionId = Convert.ToInt32(reader["DivisionId"]),
                            DistrictId = Convert.ToInt32(reader["DistrictId"]),
                            UpazilaId = Convert.ToInt32(reader["UpazilaId"]),
                            FactoryId = Convert.ToInt32(reader["FactoryId"]),
                            FactoryUnitId = Convert.ToInt32(reader["FactoryUnitId"]),
                            DesignationId = Convert.ToInt32(reader["DesignationId"]),
                            EmployeeTypeId = Convert.ToInt32(reader["EmployeeTypeId"]),
                            SalaryGradeId = Convert.ToInt32(reader["SalaryGradeId"]),
                            PresentSalary = Convert.ToInt32(reader["PresentSalary"]),
                            StartingSalary = Convert.ToInt32(reader["StartingSalary"])
                        };

                        employeeInformationDTOs.Add(employeeInformationDTO);
                    }
                }

                return employeeInformationDTOs;
            }
            catch (Exception exp)
            {
                return null;
            }
        }

        public async Task AddEmployeeInformation(EmployeeInformationModel employeeInformationModel)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(this.GetConnectionString()))
                {

                    var sp = "sp_AddEmployeeInformation";
                    SqlCommand cmd = new SqlCommand(sp, conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@EmployeeName", employeeInformationModel.EmployeeName));
                    cmd.Parameters.Add(new SqlParameter("@ContactNumber", employeeInformationModel.ContactNumber));
                    cmd.Parameters.Add(new SqlParameter("@FatherName", employeeInformationModel.FatherName));
                    cmd.Parameters.Add(new SqlParameter("@EmpMobileNo", employeeInformationModel.EmpMobileNo));
                    cmd.Parameters.Add(new SqlParameter("@NID", employeeInformationModel.NID));
                    cmd.Parameters.Add(new SqlParameter("@Photo", employeeInformationModel.Photo));
                    cmd.Parameters.Add(new SqlParameter("@JoinDate", employeeInformationModel.JoinDate));
                    cmd.Parameters.Add(new SqlParameter("@ReviewDate", employeeInformationModel.ReviewDate));
                    cmd.Parameters.Add(new SqlParameter("@CountryId", employeeInformationModel.CountryId));
                    cmd.Parameters.Add(new SqlParameter("@DivisionId", employeeInformationModel.DivisionId));
                    cmd.Parameters.Add(new SqlParameter("@DistrictId", employeeInformationModel.DistrictId));
                    cmd.Parameters.Add(new SqlParameter("@UpazilaId", employeeInformationModel.UpazilaId));
                    cmd.Parameters.Add(new SqlParameter("@FactoryId", employeeInformationModel.FactoryId));
                    cmd.Parameters.Add(new SqlParameter("@FactoryUnitId", employeeInformationModel.FactoryUnitId));
                    cmd.Parameters.Add(new SqlParameter("@DesignationId", employeeInformationModel.DesignationId));
                    cmd.Parameters.Add(new SqlParameter("@EmployeeTypeId", employeeInformationModel.EmployeeTypeId));
                    cmd.Parameters.Add(new SqlParameter("@SalaryGradeId", employeeInformationModel.SalaryGradeId));
                    cmd.Parameters.Add(new SqlParameter("@PresentSalary", employeeInformationModel.PresentSalary));
                    cmd.Parameters.Add(new SqlParameter("@StartingSalary", employeeInformationModel.StartingSalary));
                    await conn.OpenAsync();
                    var reader = await cmd.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
