﻿using DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    public interface IDivisionsDAL
    {
        public Task<List<DivisionsDTO>> GetAllDivisions();
    }
}
