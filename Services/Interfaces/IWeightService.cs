﻿using DTO;
using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IWeightService
    {
        public Task<List<WeightDTO>> GetAllWeight(int? sizeId);
        public Task AddWeight(WeightModel weightModel);
    }
}
