﻿using DTO;
using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    public interface IFactory_UnitInfoDAL
    {
        public Task<List<Factory_UnitInfoDTO>> GetAllFactory_UnitInfo();
        public Task AddFactory_UnitInfo(Factory_UnitInfoModel factory_UnitInfoModel);
    }
}
