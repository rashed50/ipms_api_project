using Autofac;
using Autofac.Extensions.DependencyInjection;
using DataAccessLayer;
using DataAccessLayer.Interfaces;
using Foundation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Services;
using Services.Interfaces;

namespace IPMSAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public static ILifetimeScope AutofacContainer { get; set; }

        public void ConfigureContainer(ContainerBuilder builder)
        {

            builder.RegisterType<CompanyInformationDAL>().As<ICompanyInformationDAL>()
               .SingleInstance();

            builder.RegisterType<CompanyInformationService>().As<ICompanyInformationService>()
               .SingleInstance();

            builder.RegisterType<FactoryInfoDAL>().As<IFactoryInfoDAL>()
               .SingleInstance();

            builder.RegisterType<FactoryInfoService>().As<IFactoryInfoService>()
              .SingleInstance();

            builder.RegisterType<EmployeeInformationDAL>().As<IEmployeeInformationDAL>()
               .SingleInstance();

            builder.RegisterType<EmployeeInformationService>().As<IEmployeeInformationService>()
                .SingleInstance();

            builder.RegisterType<Factory_UnitInfoDAL>().As<IFactory_UnitInfoDAL>()
                .SingleInstance();

            builder.RegisterType<Factory_UnitInfoService>().As<IFactory_UnitInfoService>()
                .SingleInstance();

            builder.RegisterType<EmployeeTypeDAL>().As<IEmployeeTypeDAL>()
                .SingleInstance();

            builder.RegisterType<EmployeeTypeService>().As<IEmployeeTypeService>()
                .SingleInstance();

            builder.RegisterType<CountryDAL>().As<ICountryDAL>()
                .SingleInstance();

            builder.RegisterType<CountryService>().As<ICountryService>()
                .SingleInstance();

            builder.RegisterType<DistrictsDAL>().As<IDistrictsDAL>()
                .SingleInstance();

            builder.RegisterType<DistrictsService>().As<IDistrictsService>()
                .SingleInstance();

            builder.RegisterType<DivisionsDAL>().As<IDivisionsDAL>()
                .SingleInstance();

            builder.RegisterType<DivisionsService>().As<IDivisionsService>()
                .SingleInstance();

            builder.RegisterType<UpazilaDAL>().As<IUpazilaDAL>()
                .SingleInstance();

            builder.RegisterType<UpazilaService>().As<IUpazilaService>()
                .SingleInstance();

            builder.RegisterType<ProductListDAL>().As<IProductListDAL>()
                .SingleInstance();

            builder.RegisterType<ProductListService>().As<IProductListService>()
                .SingleInstance();

            builder.RegisterType<BrandDAL>().As<IBrandDAL>()
                .SingleInstance();

            builder.RegisterType<BrandService>().As<IBrandService>()
                .SingleInstance();

            builder.RegisterType<CategoryDAL>().As<ICategoryDAL>()
                .SingleInstance();

            builder.RegisterType<CategoryService>().As<ICategoryService>()
                .SingleInstance();

            builder.RegisterType<WeightDAL>().As<IWeightDAL>()
                .SingleInstance();

            builder.RegisterType<WeightService>().As<IWeightService>()
                .SingleInstance();

            builder.RegisterType<SizeDAL>().As<ISizeDAL>()
                .SingleInstance();

            builder.RegisterType<SizeService>().As<ISizeService>()
                .SingleInstance();

           builder.RegisterType<VendorPaymentRecordDAL>().As<IVendorPaymentRecordDAL>()
               .SingleInstance();

           builder.RegisterType<VendorPaymentRecordService>().As<IVendorPaymentRecordService>()
               .SingleInstance();

            builder.RegisterType<StockDAL>().As<IStockDAL>()
              .SingleInstance();

            builder.RegisterType<StockService>().As<IStockService>()
                .SingleInstance();

            builder.RegisterType<ProductPurchaseDAL>().As<IProductPurchaseDAL>()
              .SingleInstance();

            builder.RegisterType<ProductPurchaseService>().As<IProductPurchaseService>()
                .SingleInstance();

            builder.RegisterType<VendorDAL>().As<IVendorDAL>()
              .SingleInstance();

            builder.RegisterType<VendorService>().As<IVendorService>()
                .SingleInstance();

            builder.RegisterType<CustomerInformationDAL>().As<ICustomerInformationDAL>()
              .SingleInstance();

            builder.RegisterType<CustomerInformationService>().As<ICustomerInformationService>()
                .SingleInstance();

            builder.RegisterType<PurchaseProductRecordListDAL>().As<IPurchaseProductRecordListDAL>()
              .SingleInstance();

            builder.RegisterType<PurchaseProductRecordListService>().As<IPurchaseProductRecordListService>()
                .SingleInstance();

        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();

            //services.AddSwaggerGen(c =>
            //{
            //    c.SwaggerDoc("v1", new OpenApiInfo { Title = "IPMSAPI", Version = "v1" });
            //});
            //services.AddControllersWithViews();

            services.Configure<DBConfiguration>(
               this.Configuration.GetSection("DBConnectionStrings")
            );
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(
                    builder =>
                    {
                        builder.AllowAnyOrigin().AllowAnyHeader()
                                .AllowAnyMethod();
                    });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            AutofacContainer = app.ApplicationServices.GetAutofacRoot();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                //app.UseSwagger();
                //app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "IPMSAPI v1"));

            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
