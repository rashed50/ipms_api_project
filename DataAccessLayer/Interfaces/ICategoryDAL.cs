﻿using DTO;
using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    public interface ICategoryDAL
    {
        public Task<List<CategoryDTO>> GetAllCategory();
        public Task AddCategory(CategoryModel categoryModel);
    }
}
