﻿using DataAccessLayer.Interfaces;
using DTO;
using Foundation;
using Microsoft.Extensions.Options;
using Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class StockDAL : BaseDBAccess, IStockDAL
    {
        public StockDAL(IOptions<DBConfiguration> config) : base(config)
        {

        }
        public async Task<List<StockDTO>> GetAllStock(int? stockId)
        {
            try
            {
                SqlConnection conn = new SqlConnection(this.GetConnectionString());
                conn.Open();
                var sp = "Stock_Select_SP";
                SqlCommand cmd = new SqlCommand(sp, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@StockId", stockId));
                IDataReader reader = await cmd.ExecuteReaderAsync();
                var stocks = new List<StockDTO>();

                if (reader != null)
                {
                    while (reader.Read())
                    {
                        var stock = new StockDTO
                        {
                            StockId = Convert.ToInt32(reader["StockId"]),
                            CategoryId = Convert.ToInt32(reader["CategoryId"]),
                            CategoryName = reader["CategoryName"].ToString(),
                            BrandId = Convert.ToInt32(reader["BrandId"]),
                            BrandName = reader["BrandName"].ToString(),
                            SizeId = Convert.ToInt32(reader["SizeId"]),
                            SizeName = reader["SizeName"].ToString(),
                            WeightId = Convert.ToInt32(reader["WeightId"]),
                            WeightName = reader["WeightName"].ToString(),
                            LastSaleRate = float.Parse(reader["LastSaleRate"].ToString()),
                            StockQuantity = float.Parse(reader["StockQuantity"].ToString())
                        };

                        stocks.Add(stock);
                    }
                }

                return stocks;
            }
            catch (Exception exp)
            {
                return null;
            }
        }

        public async Task AddStock(StockModel stockModel)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(this.GetConnectionString()))
                {

                    var sp = "Stock_Insert_SP";
                    SqlCommand cmd = new SqlCommand(sp, conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@CategoryId", stockModel.CategoryId));
                    cmd.Parameters.Add(new SqlParameter("@BrandId", stockModel.BrandId));
                    cmd.Parameters.Add(new SqlParameter("@SizeId", stockModel.SizeId));
                    cmd.Parameters.Add(new SqlParameter("@WeightId", stockModel.WeightId));
                    cmd.Parameters.Add(new SqlParameter("@StockQuantity", stockModel.StockQuantity));
                    await conn.OpenAsync();
                    var reader = await cmd.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<StockDTO> GetAllStockByCategoryBrandSizeWeightId(int categoryId, int brandId, int sizeId, int weightId)
        {
            try
            {
                SqlConnection conn = new SqlConnection(this.GetConnectionString());
                conn.Open();
                var sp = "Stock_Select_SP_By_Category_Brand_Size_Weight_Id";
                SqlCommand cmd = new SqlCommand(sp, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@CategoryId", categoryId));
                cmd.Parameters.Add(new SqlParameter("@BrandId", brandId));
                cmd.Parameters.Add(new SqlParameter("@SizeId", sizeId));
                cmd.Parameters.Add(new SqlParameter("@WeightId", weightId));
                IDataReader reader = await cmd.ExecuteReaderAsync();
                var stock = new StockDTO();
                stock = null;

                if (reader != null)
                {
                    while (reader.Read())
                    {
                        stock = new StockDTO()
                        {
                            StockId = Convert.ToInt32(reader["StockId"]),
                            CategoryId = Convert.ToInt32(reader["CategoryId"]),
                            CategoryName = reader["CategoryName"].ToString(),
                            BrandId = Convert.ToInt32(reader["BrandId"]),
                            BrandName = reader["BrandName"].ToString(),
                            SizeId = Convert.ToInt32(reader["SizeId"]),
                            SizeName = reader["SizeName"].ToString(),
                            WeightId = Convert.ToInt32(reader["WeightId"]),
                            WeightName = reader["WeightName"].ToString(),
                            LastSaleRate = Convert.ToInt32(reader["LastSaleRate"]),
                            StockQuantity = float.Parse(reader["StockQuantity"].ToString())
                        };
                    }
                }

                return stock;
            }
            catch (Exception exp)
            {
                return null;
            }
        }
    }
}
