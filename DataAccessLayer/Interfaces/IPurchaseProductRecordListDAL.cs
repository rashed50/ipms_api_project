﻿using DTO;
using Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    public interface IPurchaseProductRecordListDAL
    {
        public Task InsertPurchaseProductRecordList(PurchaseProductRecordListModel purchaseProductRecordListModel);
        public Task UpdatePurchaseProductRecordList(PurchaseProductRecordListModel purchaseProductRecordListModel);
        public Task DeletePurchaseProductRecordList(int id);
        public Task<List<PurchaseProductRecordListDTO>> GetPurchaseProductRecordList();
        public Task<PurchaseProductRecordListDTO> GetPurchaseProductRecordListByID(int id);
        public Task<List<PurchaseProductRecordListDTO>> GetPurchaseProductRecordListBySellReturn(int sellReturnID);
        public Task<DataTable> GetAllSellReturnSummaryList(int year);
        public Task<DataTable> GetAllSellReturnRecordList(int sellReturnId);
        public Task DeletePurchaseProductRecordListByPurchaseID(int id);
    }
}
