﻿using DTO;
using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IBrandService
    {
        public Task<List<BrandDTO>> GetAllBrand(int? categoryId);
        public Task AddBrand(BrandModel brandModel);
    }
}
