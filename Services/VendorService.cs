﻿using DataAccessLayer.Interfaces;
using DTO;
using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services
{
    public class VendorService : IVendorService
    {
        private IVendorDAL _vendorInfoDAL;
        public VendorService(IVendorDAL vendorDAL)
        {
            _vendorInfoDAL = vendorDAL;
        }

        public async Task AddVendorInfo(VendorInformationModel vendorInformationModel)
        {
            await _vendorInfoDAL.AddVendor(vendorInformationModel);
        }

        public async Task<List<VendorInformationDTO>> GetAllVendor()
        {
            var vendorList = await _vendorInfoDAL.GetAllVendor();

            return vendorList;
        }
    }
}
