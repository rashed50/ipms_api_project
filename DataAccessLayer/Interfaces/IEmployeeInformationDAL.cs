﻿using DTO;
using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    public interface IEmployeeInformationDAL
    {
        public Task<List<EmployeeInformationDTO>> GetAllEmployeeInformation();
        public Task AddEmployeeInformation(EmployeeInformationModel employeeInformationModel);
    }
}
