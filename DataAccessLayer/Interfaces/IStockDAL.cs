﻿using DTO;
using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    public interface IStockDAL
    {
        public Task<List<StockDTO>> GetAllStock(int? stockId);
        public Task AddStock(StockModel stockModel);
        public Task<StockDTO> GetAllStockByCategoryBrandSizeWeightId(int categoryId, int brandId, int sizeId, int weightId);
    }
}
