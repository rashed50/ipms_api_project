﻿namespace DTO
{
    public class CategoryDTO
    {
        public int CategoryId { get; set; }
        public bool CategoryIsActive { get; set; }
        public string CategoryName { get; set; }
    }
}
