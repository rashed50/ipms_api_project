﻿using DataAccessLayer.Interfaces;
using DTO;
using Foundation;
using Microsoft.Extensions.Options;
using Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class CategoryDAL : BaseDBAccess, ICategoryDAL
    {
        public CategoryDAL(IOptions<DBConfiguration> config) : base(config)
        {

        }
        public async Task<List<CategoryDTO>> GetAllCategory()
        {
            try
            {
                SqlConnection conn = new SqlConnection(this.GetConnectionString());
                conn.Open();
                var sp = "Category_Select_SP";
                SqlCommand cmd = new SqlCommand(sp, conn);
                IDataReader reader = await cmd.ExecuteReaderAsync();
                var categories = new List<CategoryDTO>();

                if (reader != null)
                {
                    while (reader.Read())
                    {
                        var category = new CategoryDTO
                        {
                            CategoryId = Convert.ToInt16(reader["CategoryId"]),
                            CategoryName = reader["CategoryName"].ToString(),
                            CategoryIsActive = Convert.ToBoolean(reader["CategoryIsActive"])
                        };

                        categories.Add(category);
                    }
                }

                return categories;
            }
            catch (Exception exp)
            {
                return null;
            }
        }

        public async Task AddCategory(CategoryModel categoryModel)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(this.GetConnectionString()))
                {

                    var sp = "Category_Insert_SP";
                    SqlCommand cmd = new SqlCommand(sp, conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@CategoryName", categoryModel.CategoryName));
                    cmd.Parameters.Add("@LastId", SqlDbType.Int);
                    cmd.Parameters["@LastId"].Direction = ParameterDirection.Output;
                    await conn.OpenAsync();
                    var reader = await cmd.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

