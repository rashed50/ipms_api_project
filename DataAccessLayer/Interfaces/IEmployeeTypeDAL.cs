﻿using DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    public interface IEmployeeTypeDAL
    {
        public Task<List<EmployeeTypeDTO>> GetAllEmployeeType();
        public Task<EmployeeTypeDTO> GetEmployeeTypeById(int id);
    }
}
