﻿namespace Models
{
    public class SizeModel
    {
        public string SizeName { get; set; }
        public bool SizeIsActive { get; set; }
        public int BrandId { get; set; }
    }
}