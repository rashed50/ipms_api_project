using DataAccessLayer.Interfaces;
using DTO;
using Foundation;
using Microsoft.Extensions.Options;
using System;
using System.Data;
using System.Data.SqlClient;

namespace DataAccessLayer
{
    public class CompanyInformationDAL : BaseDBAccess, ICompanyInformationDAL
    {

        public CompanyInformationDAL(IOptions<DBConfiguration> config) : base(config)
        {

        }

        public CompanyInformationDTO GetCompanyProfileInformation()
        {
            try
            {
                SqlConnection conn = new SqlConnection(this.GetConnectionString());
                conn.Open();
                SqlCommand cmd = new SqlCommand("CompanyInformation_Select_SP", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                IDataReader reader = cmd.ExecuteReader();
                CompanyInformationDTO enCompanyInformation = new CompanyInformationDTO();
                if (reader != null)
                {
                    if (reader.Read())
                    {
                        enCompanyInformation.ID = reader["ID"].ToString();
                        enCompanyInformation.ComNameEnglish = reader["ComNameEnglish"].ToString();
                        enCompanyInformation.OwnerName = reader["OwnerName"].ToString();
                        enCompanyInformation.MobileNo1 = reader["MobileNo1"].ToString();
                        enCompanyInformation.Email = reader["Email"].ToString();
                        enCompanyInformation.CompanyAddress = reader["CompanyAddress"].ToString();
                        enCompanyInformation.TradeLicense = reader["TradeLicense"].ToString();
                        enCompanyInformation.WebAddress = reader["WebAddress"].ToString();
                        enCompanyInformation.CompanyLogo = reader["CompanyLogo"].ToString();
                    }
                }
                return enCompanyInformation;
            }
            catch (Exception exp)
            {
                return null;
            }
        }

        //public bool InsertCompanyInformation(CompanyInformationEn en)
        //{
        //    try
        //    {
        //        //"CompanyInformation_Insert_SP"
        //        SqlConnection conn = new SqlConnection(this.ConnectionString);
        //        conn.Open();
        //        string sql = "insert into CompanyInformation values('"+en.TradeName+"','"+en.Title+"','"+en.CompanyAddress+"','"+en.MobileNo+"','"+en.PhoneNo+"','"+en.Fax+"','"+en.OwnerName+"','"+en.CompanyLogo+"','"+en.WebAddress+"','"+en.Email+"','"+en.SecretKey+"')";
        //        SqlCommand cmd = new SqlCommand(sql, conn);
        //        //cmd.CommandType = CommandType.StoredProcedure;
        //        //cmd.Parameters.Add(new SqlParameter("@CompanyName", enCompanyInformation.Title));
        //        //cmd.Parameters.Add(new SqlParameter("@CompanyAddress", enCompanyInformation.CompanyAddress));
        //        //cmd.Parameters.Add(new SqlParameter("@CompanyContactNo", enCompanyInformation.MobileNo));
        //        //cmd.Parameters.Add(new SqlParameter("@OwnerName", enCompanyInformation.OwnerName));
        //        //cmd.Parameters.Add(new SqlParameter("@VATonService", 5));
        //        cmd.ExecuteNonQuery();
        //        conn.Close();
        //        return true;
        //    }
        //    catch (Exception exp)
        //    {
        //        return false;
        //    }
        //}







        /*
        public void InsertCompantInfo(CompanyInformationEn enCustomerInfo)
        {
            try
            {

             

                SqlConnection conn = new SqlConnection(this.ConnectionString);
                conn.Open();
                SqlCommand cmd = new SqlCommand("CompanyInformation_Insert_SP", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@CompanyName", enCustomerInfo.TradeName));
                cmd.Parameters.Add(new SqlParameter("@Title", enCustomerInfo.Title));
                cmd.Parameters.Add(new SqlParameter("@CompanyAddress", enCustomerInfo.CompanyAddress));
                cmd.Parameters.Add(new SqlParameter("@MobileNo", enCustomerInfo.MobileNo));
                cmd.Parameters.Add(new SqlParameter("@Phone", enCustomerInfo.PhoneNo));
                cmd.Parameters.Add(new SqlParameter("@Fax", enCustomerInfo.Fax));
                cmd.Parameters.Add(new SqlParameter("@OwnerName", enCustomerInfo.OwnerName));
                 
                cmd.Parameters.Add(new SqlParameter("@Email", enCustomerInfo.Email));
                cmd.Parameters.Add(new SqlParameter("@WebAddress", enCustomerInfo.WebAddress));

                SqlParameter picParam = new SqlParameter();
                picParam.SqlDbType = SqlDbType.Image;
                picParam.ParameterName = "@CompanyLogo";
                picParam.Value = this.imageToByteArray(enCustomerInfo.CompanyLogo);
                cmd.Parameters.Add(picParam);
                cmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (Exception exp)
            {
                
            } 
            }
         

        
        public string GetSecretCodekey()
        {
            try
            {
                SqlConnection conn = new SqlConnection(this.ConnectionString);
                conn.Open();
               // string ss = "select * from CompanyInformation";
                SqlCommand cmd = new SqlCommand("select TOP 1 * from dbo.CompanyInformation", conn);
               // cmd.CommandType = CommandType.StoredProcedure;
                DataTable dt = new DataTable();
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                conn.Close();
                return dt.Rows[0]["CheckValue"].ToString(); 

            }
            catch (Exception)
            {
                return null;

            }
        }



        public bool UpdateCompanyInformation(CompanyInformationEn enCustomerInfo)
        {
            try
            {
                SqlConnection conn = new SqlConnection(this.ConnectionString);
                conn.Open();
                string sql;

                //if (en.SecretKey.Length > 0)
                //{

              

              //  sql = "update dbo.CompanyInformation set Title='" + en.Title + "', TradeName='" + en.TradeName + "',CompanyAddress='" + en.CompanyAddress + "',MobileNo='" + en.MobileNo + "',PhoneNo='" + en.PhoneNo + "',Fax='" + en.Fax + "',OwnerName='" + en.OwnerName + "',CompanyLogo='" + this.imageToByteArray(en.CompanyLogo) + "',WebAddress='" + en.WebAddress + "',Email='" + en.Email + "',CheckValue='" + en.SecretKey + "' where ID='" + en.ID + "'";

             //   }
                //else {

                //    sql = "update dbo.CompanyInformation set Title='" + en.Title + "', TradeName='" + en.TradeName + "',CompanyAddress='" + en.CompanyAddress + "',MobileNo='" + en.MobileNo + "',PhoneNo='" + en.PhoneNo + "',Fax='" + en.Fax + "',OwnerName='" + en.OwnerName + "',CompanyLogo='" + en.CompanyLogo + "',WebAddress='" + en.WebAddress + "',Email='" + en.Email + "' where ID='" + en.ID + "'";
                //} 

                SqlCommand cmd = new SqlCommand("CompanyInformation_Update_SP", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@ID", enCustomerInfo.ID));
                cmd.Parameters.Add(new SqlParameter("@CompanyName", enCustomerInfo.TradeName));
                cmd.Parameters.Add(new SqlParameter("@Title", enCustomerInfo.Title));
                cmd.Parameters.Add(new SqlParameter("@CompanyAddress", enCustomerInfo.CompanyAddress));
                cmd.Parameters.Add(new SqlParameter("@MobileNo", enCustomerInfo.MobileNo));
                cmd.Parameters.Add(new SqlParameter("@Phone", enCustomerInfo.PhoneNo));
                cmd.Parameters.Add(new SqlParameter("@Fax", enCustomerInfo.Fax));
                cmd.Parameters.Add(new SqlParameter("@OwnerName", enCustomerInfo.OwnerName));

                cmd.Parameters.Add(new SqlParameter("@Email", enCustomerInfo.Email));
                cmd.Parameters.Add(new SqlParameter("@WebAddress", enCustomerInfo.WebAddress));

                SqlParameter picParam = new SqlParameter();
                picParam.SqlDbType = SqlDbType.Image;
                picParam.ParameterName = "@CompanyLogo";
                picParam.Value = this.imageToByteArray(enCustomerInfo.CompanyLogo);
                cmd.Parameters.Add(picParam);
                cmd.ExecuteNonQuery();
                conn.Close();



                return true;
            }
            catch (Exception exp)
            {
                return false;
            }
        }


        public bool DeleteCompanyInformation(int ID)
        {
            try
            {
                SqlConnection conn = new SqlConnection(this.ConnectionString);
                conn.Open();
                SqlCommand cmd = new SqlCommand("CompanyInformation_Delete_SP", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ID", ID));
                cmd.ExecuteNonQuery();
                conn.Close();
                return true;
            }
            catch (Exception exp)
            {
                return false;
            }
        }


        //public List<CompanyInformationEn> GetCompanyInformation()
        //{
        //    try
        //    {
        //        SqlConnection conn = new SqlConnection(this.ConnectionString);
        //        conn.Open();
        //        SqlCommand cmd = new SqlCommand("CompanyInformation_Select_SP", conn);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        IDataReader reader = cmd.ExecuteReader();
        //        List<CompanyInformationEn> enCompanyInformationList = new List<CompanyInformationEn>();
        //        if (reader != null)
        //        {
        //            while (reader.Read())
        //            {
        //                CompanyInformationEn enCompanyInformation = new CompanyInformationEn();
        //                enCompanyInformation.ID = this.StringToInt(reader["ID"].ToString());
        //                //enCompanyInformation.CompanyName = reader["CompanyName"].ToString();
        //                //enCompanyInformation.CompanyAddress = reader["CompanyAddress"].ToString();
        //                //enCompanyInformation.CompanyContactNo = reader["CompanyContactNo"].ToString();
        //                //enCompanyInformation.OwnerName = reader["OwnerName"].ToString();
        //                //enCompanyInformation.VATonService = this.StringToDouble(reader["VATonService"].ToString());
        //                //enCompanyInformationList.Add(enCompanyInformation);
        //            }
        //        }
        //        reader.Close();
        //        conn.Close();
        //        return enCompanyInformationList;
        //    }
        //    catch (Exception exp)
        //    {
        //        return null;
        //    }
        //}


        public CompanyInformationEn GetCompanyInformationByID()
        {
            try
            {
                SqlConnection conn = new SqlConnection(this.ConnectionString);
                conn.Open();
                SqlCommand cmd = new SqlCommand("CompanyInformation_Select_SP", conn);
                cmd.CommandType = CommandType.StoredProcedure;
               // cmd.Parameters.Add(new SqlParameter("@ID", ID));
                IDataReader reader = cmd.ExecuteReader();
                CompanyInformationEn enCompanyInformation = new CompanyInformationEn();
                if (reader != null)
                {
                    if (reader.Read())
                    {
                        enCompanyInformation.ID = this.StringToInt(reader["ID"].ToString());
                        enCompanyInformation.TradeName = reader["TradeName"].ToString();
                        enCompanyInformation.Title = reader["Title"].ToString();
                        enCompanyInformation.CompanyAddress = reader["CompanyAddress"].ToString();
                        enCompanyInformation.MobileNo = reader["MobileNo"].ToString();
                        enCompanyInformation.PhoneNo = reader["PhonNo"].ToString();
                        enCompanyInformation.Fax = reader["Fax"].ToString();
                        enCompanyInformation.OwnerName = reader["OwnerName"].ToString();
                       // enCompanyInformation.CompanyLogo = this.StringToDouble(reader["CompanyLogo"].ToString());
                    }
                }
                return enCompanyInformation;
            }
            catch (Exception exp)
            {
                return null;
            }
        }
        public DataTable GetCompanyInformationTopOne()
        {
            try
            {
                SqlConnection conn = new SqlConnection(this.ConnectionString);
                conn.Open();
                SqlCommand cmd = new SqlCommand("CompanyInformation_SelectTopOne_SP", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                IDataReader reader = cmd.ExecuteReader();

                DataTable dtTable = new DataTable();
                dtTable.Columns.Add("TradeName");
                dtTable.Columns.Add("Title");
                dtTable.Columns.Add("CompanyAddress");
                dtTable.Columns.Add("MobilNo");
                dtTable.Columns.Add("PhonNo");
                dtTable.Columns.Add("Fax");
                dtTable.Columns.Add("OwnerName");

                while (reader.Read())
                {
                    DataRow row = dtTable.NewRow();
                    row["TradeName"] = reader["TradeName"].ToString();
                    row["Title"] = reader["Title"].ToString();
                    row["CompanyAddress"] = reader["CompanyAddress"].ToString();
                    row["MobilNo"] = reader["MobileNo"].ToString();
                    row["PhonNo"] = reader["PhonNo"].ToString();
                    row["Fax"] = reader["Fax"].ToString();
                    row["OwnerName"] = reader["OwnerName"].ToString();
                    dtTable.Rows.Add(row);
                }
                return dtTable;

            }
            catch (Exception)
            {
                return null;

            }
        }
        
        
        */
    }
}
