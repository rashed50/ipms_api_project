﻿namespace Models
{
    public class CategoryModel
    {
        public bool CategoryIsActive { get; set; }
        public string CategoryName { get; set; }
    }
}