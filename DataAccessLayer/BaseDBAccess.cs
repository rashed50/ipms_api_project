﻿using Foundation;
using Microsoft.Extensions.Options;

namespace DataAccessLayer
{
    public class BaseDBAccess
    {
        private readonly DBConfiguration _connectionStringConfig;


        public BaseDBAccess(IOptions<DBConfiguration> configAccessor)
        {
            _connectionStringConfig = configAccessor.Value;

            // Your connection string value is here:
            // _connectionStringConfig.AppDbConnection;
        }

        protected string GetConnectionString()
        {
            return _connectionStringConfig.DBConnection;
        }
    }
}
