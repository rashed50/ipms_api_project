﻿namespace DTO
{
    public class EmployeeTypeDTO
    {
        public int EmployeeTypeId { get; set; }
        public string EmployeeTypeName { get; set; }
    }
}
