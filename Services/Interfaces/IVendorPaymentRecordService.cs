﻿using DTO;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IVendorPaymentRecordService
    {
        public Task<List<VendorPaymentRecordDTO>> GetVendorPaymentRecord(int? vendorPaymentRecordId);
        public Task AddVendorPaymentRecord(VendorPaymentRecordModel vendorPaymentRecordModel);
        public Task DeleteVendorPaymentRecordById(int id);
    }
}
