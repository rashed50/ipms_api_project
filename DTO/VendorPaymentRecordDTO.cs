﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class VendorPaymentRecordDTO
    {
		public int ID { get; set; }
		public int TransactionID { get; set; }
		public int VendorID { get; set; }
		public DateTime Date { get; set; }
		public double Amount { get; set; }
		public int EntryByID { get; set; }
		public int DebitToId { get; set; }
		public int CreditToId { get; set; }
		public string CheckNo { get; set; }
		public int BankId { get; set; }
		public int PaymentTypeId { get; set; }

	}
}
