﻿using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using System.Threading.Tasks;

namespace IPMSAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UpazilaController : ControllerBase
    {
        private IUpazilaService _upazilaService;
        public UpazilaController(IUpazilaService upazilaService)
        {
            _upazilaService = upazilaService;
        }

        [HttpGet]
        // GET: UpazilaController
        public async Task<IActionResult> Get()
        {
            var upazilas = await _upazilaService.GetAllUpazila();

            if (upazilas != null)
            {
                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = upazilas
                };

                return Ok(result);
            }
            else
            {
                var result = new
                {
                    statusCode = 400,
                    success = false,
                    data = "Data Not Found",
                };

                return BadRequest(result);
            }
        }
    }
}
