﻿using DataAccessLayer.Interfaces;
using DTO;
using Foundation;
using Microsoft.Extensions.Options;
using Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class SizeDAL : BaseDBAccess, ISizeDAL
    {
        public SizeDAL(IOptions<DBConfiguration> config) : base(config)
        {

        }
        public async Task<List<SizeDTO>> GetAllSize(int? brandId)
        {
            try
            {
                SqlConnection conn = new SqlConnection(this.GetConnectionString());
                conn.Open();
                var sp = "Size_Select_SP";
                SqlCommand cmd = new SqlCommand(sp, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@BrandId", brandId));
                IDataReader reader = await cmd.ExecuteReaderAsync();
                var sizes = new List<SizeDTO>();

                if (reader != null)
                {
                    while (reader.Read())
                    {
                        var size = new SizeDTO
                        {
                            SizeId = Convert.ToInt32(reader["SizeId"]),
                            SizeName = reader["SizeName"].ToString(),
                            SizeIsActive = Convert.ToBoolean(reader["SizeIsActive"]),
                            BrandId = Convert.ToInt32(reader["BrandId"])
                        };

                        sizes.Add(size);
                    }
                }

                return sizes;
            }
            catch (Exception exp)
            {
                return null;
            }
        }

        public async Task AddSize(SizeModel sizeModel)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(this.GetConnectionString()))
                {

                    var sp = "Size_Insert_SP";
                    SqlCommand cmd = new SqlCommand(sp, conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@SizeName", sizeModel.SizeName));
                    cmd.Parameters.Add(new SqlParameter("@BrandId", sizeModel.BrandId));
                    cmd.Parameters.Add("@LastID", SqlDbType.Int);
                    cmd.Parameters["@LastID"].Direction = ParameterDirection.Output;
                    await conn.OpenAsync();
                    var reader = await cmd.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

