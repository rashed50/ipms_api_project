﻿using Foundation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Models;
using System;
using System.Threading.Tasks;

namespace IPMSAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendorController : ControllerBase
    {
        private readonly ILogger<VendorController> _logger;
        private IVendorService _vendorService;
        public VendorController(IVendorService vendorService,
          ILogger<VendorController> logger)
        {
            _vendorService = vendorService;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var vendors = await _vendorService.GetAllVendor();

            if (vendors != null)
            {
                var result = new Response
                (
                    statusCode: "200",
                    success: true,
                    data: vendors
                );

                return Ok(result);
            }
            else
            {
                var result = new Response
                (
                    statusCode: "400",
                    success: false,
                    data: "Data Not Found"
                );

                return BadRequest(result);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] VendorInformationModel vendorModel)
        {
            try
            {
                await _vendorService.AddVendorInfo(vendorModel);

                var result = new Response
                    (
                        statusCode: "200",
                        success: true,
                        data: ""
                    );

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return BadRequest();
            }
        }
    }
}
