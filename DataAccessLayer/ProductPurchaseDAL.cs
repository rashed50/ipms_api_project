﻿using DataAccessLayer.Interfaces;
using DTO;
using Foundation;
using Microsoft.Extensions.Options;
using Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class ProductPurchaseDAL : BaseDBAccess, IProductPurchaseDAL
    {
        public ProductPurchaseDAL(IOptions<DBConfiguration> config) : base(config)
        {

        }

        public async Task<int> InsertProductPurchase(ProductPurchaseModel productPurchaseModel)
        {
            SqlConnection conn = new SqlConnection(this.GetConnectionString());
            await conn.OpenAsync();
            SqlCommand cmd = new SqlCommand("ProductPurchase_Insert_SP", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add(new SqlParameter("@TransactionID", productPurchaseModel.TransactionID));
            cmd.Parameters.Add(new SqlParameter("@TotalPrice", productPurchaseModel.TotalPrice));
            cmd.Parameters.Add(new SqlParameter("@PurchaseDate", productPurchaseModel.PurchaseDate));
            cmd.Parameters.Add(new SqlParameter("@VendorID", productPurchaseModel.VendorID));
            cmd.Parameters.Add(new SqlParameter("@StaffID", productPurchaseModel.StaffID));
            cmd.Parameters.Add(new SqlParameter("@LabourCost", productPurchaseModel.LabourCost));
            cmd.Parameters.Add(new SqlParameter("@PaymentType", productPurchaseModel.PaymentTypeID));
            cmd.Parameters.Add(new SqlParameter("@BankID", productPurchaseModel.BankID));
            // cmd.Parameters.Add(new SqlParameter("@Discount", enProductPurchase.Discount));
            cmd.Parameters.Add(new SqlParameter("@CarringBill", productPurchaseModel.CarringBill));

            SqlParameter outparameter = new SqlParameter();
            outparameter.ParameterName = "@LastID";
            outparameter.Direction = ParameterDirection.Output;
            outparameter.DbType = DbType.Int32;
            cmd.Parameters.Add(outparameter);

            await cmd.ExecuteNonQueryAsync();
            Int32 _id = int.Parse(cmd.Parameters["@LastID"].Value.ToString());
            conn.Close();

            return _id;
        }

        public async Task UpdateProductPurchase(ProductPurchaseModel productPurchaseModel)
        {
            SqlConnection conn = new SqlConnection(this.GetConnectionString());
            await conn.OpenAsync();
            SqlCommand cmd = new SqlCommand("ProductPurchase_Update_SP", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add(new SqlParameter("@ID", productPurchaseModel.ID));
            cmd.Parameters.Add(new SqlParameter("@TransactionID", productPurchaseModel.TransactionID));
            cmd.Parameters.Add(new SqlParameter("@TotalPrice", productPurchaseModel.TotalPrice));
            cmd.Parameters.Add(new SqlParameter("@PurchaseDate", productPurchaseModel.PurchaseDate));
            cmd.Parameters.Add(new SqlParameter("@VendorID", productPurchaseModel.VendorID));
            cmd.Parameters.Add(new SqlParameter("@StaffID", productPurchaseModel.StaffID));
            cmd.Parameters.Add(new SqlParameter("@LabourCost", productPurchaseModel.LabourCost));

            await cmd.ExecuteNonQueryAsync();
            conn.Close();
        }

        public async Task DeleteProductPurchase(int ID)
        {
            SqlConnection conn = new SqlConnection(this.GetConnectionString());
            conn.Open();
            SqlCommand cmd = new SqlCommand("ProductPurchase_Delete_SP", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@ID", ID));
            await cmd.ExecuteNonQueryAsync();
            conn.Close();
        }

        public async Task<DataTable> GetProductPurchase()
        {
            SqlConnection conn = new SqlConnection(this.GetConnectionString());
            await conn.OpenAsync();
            SqlCommand cmd = new SqlCommand("ProductPurchase_Select_AllInfo_SP", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dt);

            return dt;
        }

        public async Task<ProductPurchaseDTO> GetProductPurchaseByID(int ID)
        {
            SqlConnection conn = new SqlConnection(this.GetConnectionString());
            await conn.OpenAsync();
            SqlCommand cmd = new SqlCommand("ProductPurchase_Select_SP", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@ID", ID));
            IDataReader reader = await cmd.ExecuteReaderAsync();

            ProductPurchaseDTO productPurchaseDTO = new ProductPurchaseDTO();
            if (reader != null)
            {
                if (reader.Read())
                {
                    productPurchaseDTO.ID = int.Parse(reader["ID"].ToString());
                    productPurchaseDTO.TransactionID = int.Parse(reader["TransactionID"].ToString());
                    productPurchaseDTO.TotalPrice = double.Parse(reader["TotalPrice"].ToString());
                    productPurchaseDTO.PurchaseDate = (DateTime)reader["PurchaseDate"];
                    productPurchaseDTO.VendorID = int.Parse(reader["VendorID"].ToString());
                    productPurchaseDTO.StaffID = int.Parse(reader["StaffID"].ToString());
                    productPurchaseDTO.LabourCost = double.Parse(reader["LabourCost"].ToString());
                }
            }

            return productPurchaseDTO;
        }

        public async Task<DataTable> GetPurchaseReportByVendorDetailByToAndFromDate(DateTime fromdate, DateTime todate)
        {
            SqlConnection conn = new SqlConnection(this.GetConnectionString());
            await conn.OpenAsync();
            SqlCommand cmd = new SqlCommand("ProductPurchaseReocrdReport_SP", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@FromDate", fromdate));
            cmd.Parameters.Add(new SqlParameter("@ToDate", todate));

            IDataReader reader = await cmd.ExecuteReaderAsync();
            DataTable dt = new DataTable();
            if (reader != null)
            {
                dt.Columns.Add("CategoryName");
                dt.Columns.Add("BrandName");
                dt.Columns.Add("SizeName");
                dt.Columns.Add("ThicknessName");
                dt.Columns.Add("VendorName");
                dt.Columns.Add("Quantity", System.Type.GetType("System.Double"));
                dt.Columns.Add("UnitPrice", System.Type.GetType("System.Double"));
                dt.Columns.Add("TotalPrice", System.Type.GetType("System.Double"));

                while (reader.Read())
                {
                    DataRow row = dt.NewRow();
                    row["Quantity"] = double.Parse(reader["Quantity"].ToString());
                    row["UnitPrice"] = double.Parse(reader["UnitPrice"].ToString());
                    row["TotalPrice"] = double.Parse(reader["Amount"].ToString());
                    row["CategoryName"] = (reader["CategoryName"].ToString());
                    row["BrandName"] = (reader["BrandName"].ToString());
                    row["SizeName"] = (reader["SizeName"].ToString());
                    row["ThicknessName"] = (reader["ThicknessName"].ToString());
                    row["VendorName"] = (reader["VendorName"].ToString());

                    dt.Rows.Add(row);
                }
            }

            reader.Close();
            await conn.CloseAsync();
            return dt;
        }

        public async Task<DataTable> GetPurchaseTransactionSummary(int vendorId, DateTime fromdate, DateTime todate)
        {
            SqlConnection conn = new SqlConnection(this.GetConnectionString());
            await conn.OpenAsync();
            SqlCommand cmd = new SqlCommand("Rpt_Vendor_Transaction_Summary", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add(new SqlParameter("@Fromdate", fromdate));
            cmd.Parameters.Add(new SqlParameter("@Todate", todate));
            if (vendorId != 0)
                cmd.Parameters.Add(new SqlParameter("@VendorId", vendorId));
            SqlDataAdapter adp = new SqlDataAdapter(cmd);

            DataTable dt = new DataTable();
            adp.Fill(dt);
            await conn.CloseAsync();

            return dt;
        }

        public async Task<DataTable> GetPurchaseReportSummary(int vendorId, DateTime fromdate, DateTime todate)
        {
            SqlConnection conn = new SqlConnection(this.GetConnectionString());
            await conn.OpenAsync();
            SqlCommand cmd = new SqlCommand("ProductPurchaseByVendorSummary", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add(new SqlParameter("@Fromdate", fromdate));
            cmd.Parameters.Add(new SqlParameter("@Todate", todate));
            if (vendorId != 0)
                cmd.Parameters.Add(new SqlParameter("@VendorID", vendorId));
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adp.Fill(dt);

            await conn.CloseAsync();
            return dt;
        }

        public async Task<DataTable> GetPurchaseSummaryByVendorId(int vendorId)
        {
            SqlConnection conn = new SqlConnection(this.GetConnectionString());
            await conn.OpenAsync();
            SqlCommand cmd = new SqlCommand("ProductPurchaseSummaryByVendorId", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add(new SqlParameter("@VendorID", vendorId));
            SqlDataAdapter adp = new SqlDataAdapter(cmd);

            DataTable dt = new DataTable();
            adp.Fill(dt);
            await conn.CloseAsync();

            return dt;
        }

        public async Task<DataTable> GetPurchaseByItemSummary(DateTime fromdate, DateTime todate, int categoryId, int brandId)
        {
            SqlConnection conn = new SqlConnection(this.GetConnectionString());
            await conn.OpenAsync();
            SqlCommand cmd = new SqlCommand("ProductPurchaseByItemSummary", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@FromDate", fromdate));
            cmd.Parameters.Add(new SqlParameter("@ToDate", todate));
            if (categoryId != 0)
                cmd.Parameters.Add(new SqlParameter("@categoryId", categoryId));
            if (brandId != 0)
                cmd.Parameters.Add(new SqlParameter("@BrandId", brandId));

            IDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            if (reader != null)
            {
                dt.Columns.Add("CategoryName");
                dt.Columns.Add("BrandName");
                dt.Columns.Add("SizeName");
                dt.Columns.Add("ThicknessName");
                dt.Columns.Add("VendorName");
                dt.Columns.Add("Quantity", System.Type.GetType("System.Double"));
                dt.Columns.Add("UnitPrice", System.Type.GetType("System.Double"));
                dt.Columns.Add("TotalPrice", System.Type.GetType("System.Double"));
                dt.Columns.Add("PurchaseDate");

                while (reader.Read())
                {
                    DataRow row = dt.NewRow();
                    row["Quantity"] = double.Parse(reader["Quantity"].ToString());
                    row["UnitPrice"] = double.Parse(reader["UnitPrice"].ToString());
                    row["TotalPrice"] = double.Parse(reader["Amount"].ToString());
                    row["CategoryName"] = (reader["CategoryName"].ToString());
                    row["BrandName"] = (reader["BrandName"].ToString());
                    row["SizeName"] = (reader["SizeName"].ToString());
                    row["ThicknessName"] = (reader["ThicknessName"].ToString());
                    row["VendorName"] = (reader["VendorName"].ToString());
                    row["PurchaseDate"] = reader["PurchaseDate"].ToString();

                    dt.Rows.Add(row);
                }
            }
            reader.Close();
            await conn.CloseAsync();
            return dt;
        }
    }
}
