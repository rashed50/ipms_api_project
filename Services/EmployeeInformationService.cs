﻿using DataAccessLayer.Interfaces;
using DTO;
using Models;
using Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services
{
    public class EmployeeInformationService : IEmployeeInformationService
    {
        private IEmployeeInformationDAL _employeeInformationDAL;
        public EmployeeInformationService(IEmployeeInformationDAL employeeInformationDAL)
        {
            _employeeInformationDAL = employeeInformationDAL;
        }

        public async Task<List<EmployeeInformationDTO>> GetAllEmployeeInformation()
        {
            return await _employeeInformationDAL.GetAllEmployeeInformation();
        }

        public async Task AddEmployeeInformation(EmployeeInformationModel employeeInformationModel)
        {
            await _employeeInformationDAL.AddEmployeeInformation(employeeInformationModel);
        }
    }
}
