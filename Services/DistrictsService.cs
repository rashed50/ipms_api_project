﻿using DataAccessLayer.Interfaces;
using DTO;
using Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services
{
    public class DistrictsService : IDistrictsService
    {
        private IDistrictsDAL _districtsDAL;
        public DistrictsService(IDistrictsDAL districtsDAL)
        {
            _districtsDAL = districtsDAL;
        }

        public async Task<List<DistrictsDTO>> GetAllDistricts()
        {
            return await _districtsDAL.GetAllDistricts();
        }
    }
}
