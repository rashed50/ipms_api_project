﻿namespace DTO
{
    public class WeightDTO
    {
        public int WeightId { get; set; }
        public string WeightName { get; set; }
        public bool WeightIsActive { get; set; }
        public int SizeId { get; set; }
    }
}
