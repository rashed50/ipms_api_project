﻿using DTO;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    public interface IVendorPaymentRecordDAL
    {
        public Task<List<VendorPaymentRecordDTO>> GetAllVendorPaymentRecord(int? vendorPaymentRecordID);
        public Task DeleteVendorPaymentRecordByID(int id);
        public Task InsertVendorPaymentRecord(VendorPaymentRecordModel vendorPaymentRecordModel);
    }
}
