﻿using Microsoft.AspNetCore.Mvc;
using Models;
using Services.Interfaces;
using System;
using System.Threading.Tasks;

namespace IPMSAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FactoryInfoController : Controller
    {
        private IFactoryInfoService _factoryInfoService;
        public FactoryInfoController(IFactoryInfoService factoryInfoService)
        {
            _factoryInfoService = factoryInfoService;
        }

        [HttpGet("getfactory")]
        // GET: FactoryInfoController
        public async Task<IActionResult> Get()
        {
            var factoryInfos = await _factoryInfoService.GetAllFactoryInfo();
            if (factoryInfos != null)
            {
                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = factoryInfos
                };

                return Ok(result);
            }
            else
            {
                var result = new
                {
                    statusCode = 400,
                    success = false,
                    data = "Data Not Found",
                };

                return BadRequest(result);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] FactoryInfoModel factoryInfoModel)
        {
            try
            {
                await _factoryInfoService.AddFactoryInfo(factoryInfoModel);

                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = ""
                };

                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new
                {
                    statusCode = 400,
                    success = false
                };

                return BadRequest(result);
            }
        }

        [HttpGet("getfactorybyid")]
        // GET: FactoryInfoController
        public async Task<IActionResult> Get([FromQuery] int id)
        {
            var factoryInfo = await _factoryInfoService.GetFactoryInfoById(id);

            if (factoryInfo != null)
            {
                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = factoryInfo
                };

                return Ok(result);
            }
            else
            {
                var result = new
                {
                    statusCode = 400,
                    success = false,
                    data = "Data Not Found",
                };

                return BadRequest(result);
            }
        }

    }
}
