﻿using System;

namespace Models
{
    class SubContactorModel
    {
        public string ContactorName { get; set; }
        public string ContactNumber { get; set; }
        public string FatherName { get; set; }
        public string ContactorNID { get; set; }
        public DateTime JoinDate { get; set; }
        public int AddressId { get; set; }
        public int EntryById { get; set; }

    }
}