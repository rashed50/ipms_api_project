﻿using DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface ICountryService
    {
        public Task<List<CountryDTO>> GetAllCountry();
    }
}
