﻿using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using System.Threading.Tasks;

namespace IPMSAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DistrictsController : ControllerBase
    {
        private IDistrictsService _districtsService;
        public DistrictsController(IDistrictsService districtsService)
        {
            _districtsService = districtsService;
        }

        [HttpGet]
        // GET: DistrictsController
        public async Task<IActionResult> Get()
        {
            var districts = await _districtsService.GetAllDistricts();

            if (districts != null)
            {
                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = districts
                };

                return Ok(result);
            }
            else
            {
                var result = new
                {
                    statusCode = 400,
                    success = false,
                    data = "Data Not Found",
                };

                return BadRequest(result);
            }
        }
    }
}
