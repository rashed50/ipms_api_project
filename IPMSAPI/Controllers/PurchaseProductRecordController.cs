﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models;
using Newtonsoft.Json;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPMSAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PurchaseProductRecordController : ControllerBase
    {
        private IPurchaseProductRecordListService _purchaseProductRecordListService;
        public PurchaseProductRecordController(IPurchaseProductRecordListService purchaseProductRecordListService)
        {
            _purchaseProductRecordListService = purchaseProductRecordListService;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] PurchaseProductRecordListModel purchaseProductRecordListModel)
        {
            try
            {
                await _purchaseProductRecordListService.InsertPurchaseProductRecordList(purchaseProductRecordListModel);

                var result = new
                {
                    statusCode = 400,
                    success = true
                };

                return Ok(result);
            }
            catch
            {
                var result = new
                {
                    statusCode = 200,
                    success = false
                };

                return BadRequest(result);
            }
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody] PurchaseProductRecordListModel purchaseProductRecordListModel)
        {
            try
            {
                await _purchaseProductRecordListService.UpdatePurchaseProductRecordList(purchaseProductRecordListModel);

                var result = new
                {
                    statusCode = 200,
                    success = true
                };

                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new
                {
                    statusCode = 400,
                    success = false
                };

                return BadRequest(result);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _purchaseProductRecordListService.DeletePurchaseProductRecordList(id);

                var result = new
                {
                    statusCode = 200,
                    success = true
                };

                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new
                {
                    statusCode = 400,
                    success = false
                };

                return BadRequest(result);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var purchaseProductRecordLists = await _purchaseProductRecordListService.GetPurchaseProductRecordList();

                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = purchaseProductRecordLists
                };

                return Ok(result);
            }
            catch
            {
                var result = new
                {
                    statusCode = 400,
                    success = false,
                    data = "Data Not Found",
                };

                return BadRequest(result);
            }
        }

        [HttpGet("GetPurchaseProductRecordListByID/{id}")]
        public async Task<IActionResult> GetPurchaseProductRecordListByID(int id)
        {           
            try
            {
                var purchaseProductRecord = await _purchaseProductRecordListService.GetPurchaseProductRecordListByID(id);

                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = purchaseProductRecord
                };

                return Ok(result);
            }
            catch
            {
                var result = new
                {
                    statusCode = 400,
                    success = false,
                    data = "Data Not Found",
                };

                return BadRequest(result);
            }
        }

        [HttpGet("GetPurchaseProductRecordListBySellReturn/{sellReturnID}")]
        public async Task<IActionResult> GetPurchaseProductRecordListBySellReturn(int sellReturnID)
        {            
            try
            {
                var purchaseProductRecordList = await _purchaseProductRecordListService.GetPurchaseProductRecordListBySellReturn(sellReturnID);

                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = purchaseProductRecordList
                };

                return Ok(result);
            }
            catch
            {
                var result = new
                {
                    statusCode = 400,
                    success = false,
                    data = "Data Not Found",
                };

                return BadRequest(result);
            }
        }

        [HttpGet("GetAllSellReturnSummaryList/{year}")]
        public async Task<IActionResult> GetAllSellReturnSummaryList(int year)
        {            
            try
            {
                var purchaseProductRecordDatatable = await _purchaseProductRecordListService.GetAllSellReturnSummaryList(year);
                var list = JsonConvert.SerializeObject(purchaseProductRecordDatatable);

                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = list
                };

                return Ok(result);
            }
            catch
            {
                var result = new
                {
                    statusCode = 400,
                    success = false,
                    data = "Data Not Found",
                };

                return BadRequest(result);
            }
        }

        [HttpGet("GetAllSellReturnRecordList/{sellReturnId}")]
        public async Task<IActionResult> GetAllSellReturnRecordList(int sellReturnId)
        {            
            try
            {
                var purchaseProductRecordDatatable = await _purchaseProductRecordListService.GetAllSellReturnRecordList(sellReturnId);
                var list = JsonConvert.SerializeObject(purchaseProductRecordDatatable);

                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = list
                };

                return Ok(result);
            }
            catch
            {
                var result = new
                {
                    statusCode = 400,
                    success = false,
                    data = "Data Not Found",
                };

                return BadRequest(result);
            }
        }

        [HttpDelete("DeletePurchaseProductRecordListByPurchaseID/{id}")]
        public async Task<IActionResult> DeletePurchaseProductRecordListByPurchaseID(int id)
        {
            try
            {
                await _purchaseProductRecordListService.DeletePurchaseProductRecordListByPurchaseID(id);

                var result = new
                {
                    statusCode = 200,
                    success = true
                };

                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new
                {
                    statusCode = 400,
                    success = false
                };

                return BadRequest(result);
            }
        }
    }
}
