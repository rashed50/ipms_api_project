﻿using DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    public interface IDistrictsDAL
    {
        public Task<List<DistrictsDTO>> GetAllDistricts();
    }
}
