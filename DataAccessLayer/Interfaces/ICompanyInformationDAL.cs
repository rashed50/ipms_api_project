﻿using DTO;

namespace DataAccessLayer.Interfaces
{
    public interface ICompanyInformationDAL
    {
        public CompanyInformationDTO GetCompanyProfileInformation();
    }
}
