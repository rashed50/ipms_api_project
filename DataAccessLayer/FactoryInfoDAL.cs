﻿using DataAccessLayer.Interfaces;
using DTO;
using Foundation;
using Microsoft.Extensions.Options;
using Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class FactoryInfoDAL : BaseDBAccess, IFactoryInfoDAL
    {
        public FactoryInfoDAL(IOptions<DBConfiguration> config) : base(config)
        {

        }

        public async Task AddFactoryInfo(FactoryInfoModel factoryInfoModel)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(this.GetConnectionString()))
                {

                    var sp = "sp_AddFactoryInfo";
                    SqlCommand cmd = new SqlCommand(sp, conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@FactoryEmail", factoryInfoModel.FactoryEmail));
                    cmd.Parameters.Add(new SqlParameter("@FactoryAddress", factoryInfoModel.FactoryAddress));
                    cmd.Parameters.Add(new SqlParameter("@FactoryPhoto1", factoryInfoModel.FactoryPhoto1));
                    cmd.Parameters.Add(new SqlParameter("@FactoryName", factoryInfoModel.FactoryName));
                    cmd.Parameters.Add(new SqlParameter("@FactoryMobileNo", factoryInfoModel.FactoryMobileNo));
                    cmd.Parameters.Add(new SqlParameter("@FactoryIsActive", factoryInfoModel.FactoryIsActive));
                    await conn.OpenAsync();
                    var reader = await cmd.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<FactoryInfoDTO>> GetAllFactoryInfo()
        {
            try
            {
                SqlConnection conn = new SqlConnection(this.GetConnectionString());
                conn.Open();
                SqlCommand cmd = new SqlCommand("Select * from FactoryInfo", conn);
                IDataReader reader = await cmd.ExecuteReaderAsync();
                var factoryInfoDTOs = new List<FactoryInfoDTO>();
                if (reader != null)
                {
                    while (reader.Read())
                    {
                        var factoryInfoDTO = new FactoryInfoDTO
                        {
                            FactoryId = Convert.ToInt32(reader["FactoryId"]),
                            FactoryName = reader["FactoryName"].ToString(),
                            FactoryAddress = reader["FactoryAddress"].ToString(),
                            FactoryEmail = reader["FactoryEmail"].ToString(),
                            FactoryMobile = reader["FactoryMobileNo"].ToString(),
                            FactoryPhoto1 = reader["FactoryPhoto1"].ToString(),
                            FactoryIsActive = Convert.ToBoolean(reader["FactoryIsActive"])
                        };

                        factoryInfoDTOs.Add(factoryInfoDTO);
                    }
                }

                return factoryInfoDTOs;
            }
            catch (Exception exp)
            {
                return null;
            }
        }

        public async Task<FactoryInfoDTO> GetFactoryInfoById(int id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(this.GetConnectionString());
                conn.Open();
                SqlCommand cmd = new SqlCommand($"Select * from FactoryInfo where FactoryId={id}", conn);
                IDataReader reader = await cmd.ExecuteReaderAsync();
                var factoryInfoDTO = new FactoryInfoDTO();
                factoryInfoDTO = null;

                if (reader != null)
                {
                    while (reader.Read())
                    {
                        factoryInfoDTO = new FactoryInfoDTO
                        {
                            FactoryId = Convert.ToInt32(reader["FactoryId"]),
                            FactoryName = reader["FactoryName"].ToString(),
                            FactoryAddress = reader["FactoryAddress"].ToString(),
                            FactoryEmail = reader["FactoryEmail"].ToString(),
                            FactoryMobile = reader["FactoryMobileNo"].ToString(),
                            FactoryPhoto1 = reader["FactoryPhoto1"].ToString(),
                            FactoryIsActive = Convert.ToBoolean(reader["FactoryIsActive"])
                        };
                    }
                }

                return factoryInfoDTO;
            }
            catch (Exception exp)
            {
                return null;
            }
        }
    }
}
