﻿using DataAccessLayer.Interfaces;
using DTO;
using Models;
using Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services
{
    public class CategoryService : ICategoryService
    {
        private ICategoryDAL _categoryDAL;
        public CategoryService(ICategoryDAL categoryDAL)
        {
            _categoryDAL = categoryDAL;
        }

        public async Task<List<CategoryDTO>> GetAllCategory()
        {
            return await _categoryDAL.GetAllCategory();
        }

        public async Task AddCategory(CategoryModel categoryModel)
        {
            await _categoryDAL.AddCategory(categoryModel);
        }
    }
}
