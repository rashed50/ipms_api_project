﻿using DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IDivisionsService
    {
        public Task<List<DivisionsDTO>> GetAllDivisions();
    }
}
