﻿using DataAccessLayer.Interfaces;
using DTO;
using Models;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services
{
    public class CustomerInformationService : ICustomerInformationService
    {
        private ICustomerInformationDAL _customerInformationDAL;
        public CustomerInformationService(ICustomerInformationDAL customerInformationDAL)
        {
            _customerInformationDAL = customerInformationDAL;
        }
        public async Task<List<CustomerInformationDTO>> GetAllCustomers(int? customerType = null)
        {
            var customers = await _customerInformationDAL.GetAllCustomer(customerType);

            return customers;
        }

        public async Task AddCustomer(CustomerInformationModel model)
        {
            await _customerInformationDAL.AddCustomer(model);
        }
    }
}
