﻿using DTO;
using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IEmployeeInformationService
    {
        public Task<List<EmployeeInformationDTO>> GetAllEmployeeInformation();
        public Task AddEmployeeInformation(EmployeeInformationModel employeeInformationModel);
    }
}
