﻿namespace DTO
{
    public class DistrictsDTO
    {
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
        public int DivisionId { get; set; }
    }
}
