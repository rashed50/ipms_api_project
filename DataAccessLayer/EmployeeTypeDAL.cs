﻿using DataAccessLayer.Interfaces;
using DTO;
using Foundation;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class EmployeeTypeDAL : BaseDBAccess, IEmployeeTypeDAL
    {
        public EmployeeTypeDAL(IOptions<DBConfiguration> config) : base(config)
        {

        }
        public async Task<List<EmployeeTypeDTO>> GetAllEmployeeType()
        {
            try
            {
                SqlConnection conn = new SqlConnection(this.GetConnectionString());
                conn.Open();
                SqlCommand cmd = new SqlCommand("Select * from EmployeeType", conn);
                IDataReader reader = await cmd.ExecuteReaderAsync();
                var employeeTypeDTOs = new List<EmployeeTypeDTO>();

                if (reader != null)
                {
                    while (reader.Read())
                    {
                        var employeeTypeDTO = new EmployeeTypeDTO
                        {
                            EmployeeTypeId = Convert.ToInt32(reader["EmployeeTypeId"]),
                            EmployeeTypeName = reader["EmployeeTypeName"].ToString(),
                        };

                        employeeTypeDTOs.Add(employeeTypeDTO);
                    }
                }

                return employeeTypeDTOs;
            }
            catch (Exception exp)
            {
                return null;
            }
        }

        public async Task<EmployeeTypeDTO> GetEmployeeTypeById(int id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(this.GetConnectionString());
                conn.Open();
                SqlCommand cmd = new SqlCommand($"Select * from EmployeeType where employeetypeid={id}", conn);
                IDataReader reader = await cmd.ExecuteReaderAsync();
                var employeeTypeDTO = new EmployeeTypeDTO();
                employeeTypeDTO = null;

                if (reader != null)
                {
                    while (reader.Read())
                    {
                        employeeTypeDTO = new EmployeeTypeDTO
                        {
                            EmployeeTypeId = Convert.ToInt32(reader["EmployeeTypeId"]),
                            EmployeeTypeName = reader["EmployeeTypeName"].ToString(),
                        };
                    }
                }

                return employeeTypeDTO;
            }
            catch (Exception exp)
            {
                return null;
            }
        }
    }
}
