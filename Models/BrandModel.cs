﻿namespace Models
{
    public class BrandModel
    {
        public string BrandName { get; set; }
        public bool BrandIsActive { get; set; }
        public int CategoryId { get; set; }
    }
}
