﻿using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using System.Threading.Tasks;

namespace IPMSAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DivisionsController : ControllerBase
    {
        private IDivisionsService _divisionsService;
        public DivisionsController(IDivisionsService divisionsService)
        {
            _divisionsService = divisionsService;
        }

        [HttpGet]
        // GET: DistrictsController
        public async Task<IActionResult> Get()
        {
            var divisions = await _divisionsService.GetAllDivisions();

            if (divisions != null)
            {
                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = divisions
                };

                return Ok(result);
            }
            else
            {
                var result = new
                {
                    statusCode = 400,
                    success = false,
                    data = "Data Not Found",
                };

                return BadRequest(result);
            }
        }
    }
}
