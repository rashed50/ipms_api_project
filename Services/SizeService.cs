﻿using DataAccessLayer.Interfaces;
using DTO;
using Models;
using Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services
{
    public class SizeService : ISizeService
    {
        private ISizeDAL _sizeDAL;
        public SizeService(ISizeDAL sizeDAL)
        {
            _sizeDAL = sizeDAL;
        }

        public async Task<List<SizeDTO>> GetAllSize(int? brandId)
        {
            return await _sizeDAL.GetAllSize(brandId);
        }

        public async Task AddSize(SizeModel sizeModel)
        {
            await _sizeDAL.AddSize(sizeModel);
        }
    }
}
