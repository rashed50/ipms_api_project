using DTO;
using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

public interface IVendorService
{
    public Task AddVendorInfo(VendorInformationModel vendorInformationModel);
    public Task<List<VendorInformationDTO>> GetAllVendor();
}