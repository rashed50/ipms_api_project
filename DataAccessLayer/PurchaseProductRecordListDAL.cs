﻿using DataAccessLayer.Interfaces;
using DTO;
using Foundation;
using Microsoft.Extensions.Options;
using Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class PurchaseProductRecordListDAL : BaseDBAccess, IPurchaseProductRecordListDAL
    {
        public PurchaseProductRecordListDAL(IOptions<DBConfiguration> config) : base(config)
        {

        }

        public async Task InsertPurchaseProductRecordList(PurchaseProductRecordListModel purchaseProductRecordListModel)
        {
            SqlConnection conn = new SqlConnection(this.GetConnectionString());
            await conn.OpenAsync();
            SqlCommand cmd = new SqlCommand("PurchaseProductRecordList_Insert_SP", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@ProductListID", purchaseProductRecordListModel.ProductListID));
            cmd.Parameters.Add(new SqlParameter("@Quantity", purchaseProductRecordListModel.Quantity));
            cmd.Parameters.Add(new SqlParameter("@UnitPrice", purchaseProductRecordListModel.UnitPrice));
            cmd.Parameters.Add(new SqlParameter("@ProductPurchaseID", purchaseProductRecordListModel.ProductPurchaseID));
            cmd.Parameters.Add(new SqlParameter("@Amount", purchaseProductRecordListModel.Amount));

            await cmd.ExecuteNonQueryAsync();
            conn.Close();
        }

        public async Task UpdatePurchaseProductRecordList(PurchaseProductRecordListModel purchaseProductRecordListModel)
        {
            SqlConnection conn = new SqlConnection(this.GetConnectionString());
            await conn.OpenAsync();
            SqlCommand cmd = new SqlCommand("PurchaseProductRecordList_Update_SP", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@ID", purchaseProductRecordListModel.ID));
            cmd.Parameters.Add(new SqlParameter("@ProductListID", purchaseProductRecordListModel.ProductListID));
            cmd.Parameters.Add(new SqlParameter("@Quantity", purchaseProductRecordListModel.Quantity));
            cmd.Parameters.Add(new SqlParameter("@UnitPrice", purchaseProductRecordListModel.UnitPrice));
            cmd.Parameters.Add(new SqlParameter("@ProductPurchaseID", purchaseProductRecordListModel.ProductPurchaseID));
            cmd.Parameters.Add(new SqlParameter("@Amount", purchaseProductRecordListModel.Amount));

            await cmd.ExecuteNonQueryAsync();
            conn.Close();
        }

        public async Task DeletePurchaseProductRecordList(int id)
        {
            SqlConnection conn = new SqlConnection(this.GetConnectionString());
            await conn.OpenAsync();
            SqlCommand cmd = new SqlCommand("PurchaseProductRecordList_Delete_SP", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@ID", id));

            await cmd.ExecuteNonQueryAsync();
            conn.Close();
        }

        public async Task<List<PurchaseProductRecordListDTO>> GetPurchaseProductRecordList()
        {

            SqlConnection conn = new SqlConnection(this.GetConnectionString());
            await conn.OpenAsync();
            SqlCommand cmd = new SqlCommand("PurchaseProductRecordList_Select_SP", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            IDataReader reader = await cmd.ExecuteReaderAsync();

            List<PurchaseProductRecordListDTO> purchaseProductRecordListDTOs = new List<PurchaseProductRecordListDTO>();
            if (reader != null)
            {
                while (reader.Read())
                {
                    PurchaseProductRecordListDTO purchaseProductRecordListDTO = new PurchaseProductRecordListDTO();
                    purchaseProductRecordListDTO.PurchaseDate = Convert.ToDateTime(reader["PurchaseDate"].ToString());
                    int monthdif = System.DateTime.Now.Month - Convert.ToDateTime(purchaseProductRecordListDTO.PurchaseDate).Month;

                    if (monthdif <= 6)
                    {
                        purchaseProductRecordListDTO.ID = int.Parse(reader["ID"].ToString());
                        purchaseProductRecordListDTO.ProductPurchaseID = int.Parse(reader["ProductPurchaseID"].ToString());
                        purchaseProductRecordListDTO.TransactionID = int.Parse(reader["TransactionID"].ToString());
                        purchaseProductRecordListDTO.Quantity = double.Parse(reader["Quantity"].ToString());
                        purchaseProductRecordListDTO.UnitPrice = double.Parse(reader["UnitPrice"].ToString());
                        purchaseProductRecordListDTO.Amount = double.Parse(reader["Amount"].ToString());
                        purchaseProductRecordListDTO.TotalAmount = double.Parse(reader["TotalAmount"].ToString());
                        purchaseProductRecordListDTO.ProductCatgoryName = (reader["CategoryName"].ToString());
                        purchaseProductRecordListDTO.ProductBrandName = (reader["BrandName"].ToString());
                        purchaseProductRecordListDTO.ProductSizeName = (reader["SizeName"].ToString());
                        purchaseProductRecordListDTO.ProductThickness = (reader["ThicknessName"].ToString());
                        purchaseProductRecordListDTO.VendorName = (reader["VendorName"].ToString());                                     
                        purchaseProductRecordListDTO.StaffName = (reader["staffName"].ToString());

                        purchaseProductRecordListDTOs.Add(purchaseProductRecordListDTO);
                    }
                }
            }

            reader.Close();
            conn.Close();

            return purchaseProductRecordListDTOs;
        }

        public async Task<PurchaseProductRecordListDTO> GetPurchaseProductRecordListByID(int id)
        {
            SqlConnection conn = new SqlConnection(this.GetConnectionString());
            await conn.OpenAsync();
            SqlCommand cmd = new SqlCommand("PurchaseProductRecordList_Select_SP", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("ID", id));
            IDataReader reader = await cmd.ExecuteReaderAsync();
               
            PurchaseProductRecordListDTO purchaseProductRecordListDTO = new PurchaseProductRecordListDTO();
            if (reader != null)
            {
                if (reader.Read())
                {

                    purchaseProductRecordListDTO.ID = int.Parse(reader["ID"].ToString());
                    purchaseProductRecordListDTO.ProductPurchaseID = int.Parse(reader["ProductPurchaseID"].ToString());
                    purchaseProductRecordListDTO.TransactionID = int.Parse(reader["TransactionID"].ToString());
                    purchaseProductRecordListDTO.Quantity = double.Parse(reader["Quantity"].ToString());
                    purchaseProductRecordListDTO.UnitPrice = double.Parse(reader["UnitPrice"].ToString());
                    purchaseProductRecordListDTO.Amount = double.Parse(reader["Amount"].ToString());
                    purchaseProductRecordListDTO.PurchaseDate = Convert.ToDateTime(reader["PurchaseDate"].ToString());
                    purchaseProductRecordListDTO.TotalAmount = double.Parse(reader["TotalAmount"].ToString());
                    purchaseProductRecordListDTO.ProductCatgoryName = (reader["CategoryName"].ToString());
                    purchaseProductRecordListDTO.ProductBrandName = (reader["BrandName"].ToString());
                    purchaseProductRecordListDTO.ProductSizeName = (reader["SizeName"].ToString());
                    purchaseProductRecordListDTO.ProductThickness = (reader["ThicknessName"].ToString());
                    purchaseProductRecordListDTO.VendorName = (reader["VendorName"].ToString());
                    purchaseProductRecordListDTO.StaffName = (reader["staffName"].ToString());
                }
            }
            reader.Close();
            conn.Close();

            return purchaseProductRecordListDTO;
        }

        public async Task<List<PurchaseProductRecordListDTO>> GetPurchaseProductRecordListBySellReturn(int sellReturnID)
        {
            SqlConnection conn = new SqlConnection(this.GetConnectionString());
            await conn.OpenAsync();     
            SqlCommand cmd = new SqlCommand("[dbo].[sellReturn_Record_lst]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@SellReturnID", sellReturnID));
            IDataReader reader = await cmd.ExecuteReaderAsync();

            List<PurchaseProductRecordListDTO> purchaseProductRecordListDTOs = new List<PurchaseProductRecordListDTO>();
            if (reader != null)
            {
                while (reader.Read())
                {
                    PurchaseProductRecordListDTO purchaseProductRecordListDTO = new PurchaseProductRecordListDTO();
                    purchaseProductRecordListDTO.ID = int.Parse(reader["ID"].ToString());
                    purchaseProductRecordListDTO.ProductPurchaseID = int.Parse(reader["ProductPurchaseID"].ToString());
                    purchaseProductRecordListDTO.TransactionID = int.Parse(reader["TransactionID"].ToString());
                    purchaseProductRecordListDTO.Quantity = double.Parse(reader["Quantity"].ToString());
                    purchaseProductRecordListDTO.UnitPrice = double.Parse(reader["UnitPrice"].ToString());
                    purchaseProductRecordListDTO.Amount = double.Parse(reader["Amount"].ToString());
                    purchaseProductRecordListDTO.PurchaseDate = Convert.ToDateTime(reader["PurchaseDate"].ToString());
                    purchaseProductRecordListDTO.TotalAmount = double.Parse(reader["TotalAmount"].ToString());
                    purchaseProductRecordListDTO.ProductCatgoryName = (reader["CategoryName"].ToString());
                    purchaseProductRecordListDTO.ProductBrandName = (reader["BrandName"].ToString());
                    purchaseProductRecordListDTO.ProductSizeName = (reader["SizeName"].ToString());
                    purchaseProductRecordListDTO.ProductThickness = (reader["ThicknessName"].ToString());
                    purchaseProductRecordListDTO.VendorName = (reader["VendorName"].ToString());
                    purchaseProductRecordListDTO.StaffName = (reader["staffName"].ToString());
                    purchaseProductRecordListDTO.ProductListID = Convert.ToInt32(reader["ProductListID"].ToString());

                    purchaseProductRecordListDTOs.Add(purchaseProductRecordListDTO);
                }
            }

            reader.Close();
            conn.Close();

            return purchaseProductRecordListDTOs;
        }

        public async Task<DataTable> GetAllSellReturnSummaryList(int year)
        {
            SqlConnection conn = new SqlConnection(this.GetConnectionString());
            await conn.OpenAsync();        
            SqlCommand cmd = new SqlCommand("[dbo].[sellReturn_lst]", conn);
            cmd.Parameters.Add(new SqlParameter("@year", year));
            cmd.CommandType = CommandType.StoredProcedure;

            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dt);

            return dt;
        }

        public async Task<DataTable> GetAllSellReturnRecordList(int sellReturnId)
        {
            SqlConnection conn = new SqlConnection(this.GetConnectionString());
            await conn.OpenAsync();
            SqlCommand cmd = new SqlCommand("[dbo].[sellReturn_Record_lst]", conn);
            cmd.Parameters.Add(new SqlParameter("@sellReturnId", sellReturnId));
            cmd.CommandType = CommandType.StoredProcedure;

            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dt);

            return dt;
        }

        public async Task DeletePurchaseProductRecordListByPurchaseID(int id)
        {
            SqlConnection conn = new SqlConnection(this.GetConnectionString());
            await conn.OpenAsync();
            SqlCommand cmd = new SqlCommand("PurchaseProductRecordList_DeleteBy_ProductPurchaseID_SP", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@ProductPurchaseID", id));

            await cmd.ExecuteNonQueryAsync();
            conn.Close();
        }
    }    
}
