﻿namespace DTO
{
    public class SizeDTO
    {
        public int SizeId { get; set; }
        public string SizeName { get; set; }
        public bool SizeIsActive { get; set; }
        public int BrandId { get; set; }
    }
}
