﻿using DataAccessLayer.Interfaces;
using DTO;
using Models;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class VendorPaymentRecordService : IVendorPaymentRecordService
    {
        private IVendorPaymentRecordDAL _vendorPaymentRecordDAL;
        public VendorPaymentRecordService(IVendorPaymentRecordDAL vendorPaymentRecordDAL)
        {
            _vendorPaymentRecordDAL = vendorPaymentRecordDAL;
        }

        public async Task<List<VendorPaymentRecordDTO>> GetVendorPaymentRecord(int? vendorPaymentRecordId)
        {
            return await _vendorPaymentRecordDAL.GetAllVendorPaymentRecord(vendorPaymentRecordId);
        }

        public async Task AddVendorPaymentRecord(VendorPaymentRecordModel vendorPaymentRecordModel)
        {
            await _vendorPaymentRecordDAL.InsertVendorPaymentRecord(vendorPaymentRecordModel);
        }

        public async Task DeleteVendorPaymentRecordById(int id)
        {
            await _vendorPaymentRecordDAL.DeleteVendorPaymentRecordByID(id);
        }
    }
}
