﻿using DataAccessLayer.Interfaces;
using DTO;
using Models;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services
{
    public class Factory_UnitInfoService : IFactory_UnitInfoService
    {
        private IFactory_UnitInfoDAL _factory_UnitInfoDAL;
        private IFactoryInfoDAL _factoryInfoDAL;
        public Factory_UnitInfoService(IFactory_UnitInfoDAL factory_UnitInfoDAL, IFactoryInfoDAL factoryInfoDAL)
        {
            _factory_UnitInfoDAL = factory_UnitInfoDAL;
            _factoryInfoDAL = factoryInfoDAL;
        }
        public async Task<List<Factory_UnitInfoDTO>> GetAllFactory_UnitInfo()
        {
            return await _factory_UnitInfoDAL.GetAllFactory_UnitInfo();

        }

        public async Task AddFactory_UnitInfo(Factory_UnitInfoModel factory_UnitInfoModel)
        {
            var factory = _factoryInfoDAL.GetFactoryInfoById(factory_UnitInfoModel.FactoryId);
            if (factory == null)
                throw new Exception("Factory not found!");

            await _factory_UnitInfoDAL.AddFactory_UnitInfo(factory_UnitInfoModel);
        }
    }
}
