﻿using DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IDistrictsService
    {
        public Task<List<DistrictsDTO>> GetAllDistricts();
    }
}
