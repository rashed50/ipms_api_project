﻿namespace DTO
{
    public class ProductListDTO
    {
        public int ProductListId { get; set; }
        public int CategoryId { get; set; }
        public int BrandId { get; set; }
        public int SizeId { get; set; }
        public int WeightId { get; set; }
        public double tempPRate { get; set; }
        public bool IsProductActive { get; set; }
        public string CategoryName { get; set; }
        public string BrandName { get; set; }
        public string SizeName { get; set; }
        public string WeightName { get; set; }
    }
}
