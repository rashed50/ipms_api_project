﻿using DataAccessLayer.Interfaces;
using DTO;
using Foundation;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class UpazilaDAL : BaseDBAccess, IUpazilaDAL
    {
        public UpazilaDAL(IOptions<DBConfiguration> config) : base(config)
        {

        }
        public async Task<List<UpazilaDTO>> GetAllUpazila()
        {
            try
            {
                SqlConnection conn = new SqlConnection(this.GetConnectionString());
                conn.Open();
                var sp = "sp_GetUpazila";
                SqlCommand cmd = new SqlCommand(sp, conn);
                IDataReader reader = await cmd.ExecuteReaderAsync();
                var upazilas = new List<UpazilaDTO>();

                if (reader != null)
                {
                    while (reader.Read())
                    {
                        var upazila = new UpazilaDTO
                        {
                            UpazilaId = Convert.ToInt32(reader["UpazilaId"]),
                            UpazilaName = reader["UpazilaName"].ToString(),
                            DistrictId = Convert.ToInt32(reader["DistrictId"])
                        };

                        upazilas.Add(upazila);
                    }
                }

                return upazilas;
            }
            catch (Exception exp)
            {
                return null;
            }
        }
    }
}
