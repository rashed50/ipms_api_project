﻿using DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IEmployeeTypeService
    {
        public Task<List<EmployeeTypeDTO>> GetAllEmployeeType();
        public Task<EmployeeTypeDTO> GetEmployeeTypeById(int id);
    }
}
