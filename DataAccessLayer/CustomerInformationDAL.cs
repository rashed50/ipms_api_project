﻿using DataAccessLayer.Interfaces;
using DTO;
using Foundation;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;

namespace DataAccessLayer
{
    public class CustomerInformationDAL : BaseDBAccess, ICustomerInformationDAL
    {
        public CustomerInformationDAL(IOptions<DBConfiguration> config) : base(config)
        {

        }

        public async Task AddCustomer(CustomerInformationModel model)
        {
            using (SqlConnection conn = new SqlConnection(this.GetConnectionString()))
            {

                var sp = "CustomerInfo_Insert_SP";
                SqlCommand cmd = new SqlCommand(sp, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Name", model.Name));
                cmd.Parameters.Add(new SqlParameter("@FatherName", model.FatherName));
                cmd.Parameters.Add(new SqlParameter("@TradeName", model.TradeName));
                cmd.Parameters.Add(new SqlParameter("@ContactNumber", model.ContactNumber));
                cmd.Parameters.Add(new SqlParameter("@Address", model.Address));
                cmd.Parameters.Add(new SqlParameter("@NID", model.NID));
                cmd.Parameters.Add(new SqlParameter("@CustomerTypeID", model.CustomerTypeId));
                cmd.Parameters.Add(new SqlParameter("@DueAmount", model.DueAmount));
                cmd.Parameters.Add(new SqlParameter("@InitialDue", model.InitialDueAmount));
                cmd.Parameters.Add(new SqlParameter("@Photos", model.Photo));
                cmd.Parameters.Add(new SqlParameter("@CountryId", model.CountryId));
                cmd.Parameters.Add(new SqlParameter("@DivisionId", model.DivisionId));
                cmd.Parameters.Add(new SqlParameter("@DistrictId", model.DistrictId));
                cmd.Parameters.Add(new SqlParameter("@UpazillaId", model.UpazillaId));
                cmd.Parameters.Add(new SqlParameter("@EntryById", model.EntryById));

                await conn.OpenAsync();
                var reader = await cmd.ExecuteNonQueryAsync();
            }
        }

        public async Task<List<CustomerInformationDTO>> GetAllCustomer(int? customerType = null)
        {
            var customerInfoList = new List<CustomerInformationDTO>();

            try 
            { 
                using SqlConnection conn = new SqlConnection(this.GetConnectionString());
                conn.Open();

                var sp = "CustomerInfo_Select_SP";
                SqlCommand cmd = new SqlCommand(sp, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@CustomerType", customerType));

                using var reader = await cmd.ExecuteReaderAsync();
                if (reader != null)
                {
                    while (reader.Read())
                    {

                        var customerInfo = new CustomerInformationDTO();

                        customerInfo.Id = Convert.ToInt32(reader["ID"].ToString());
                        customerInfo.Name = reader["Name"].ToString();
                        customerInfo.TradeName = reader["TradeName"].ToString();
                        customerInfo.ContactNumber = reader["ContactNumber"].ToString();
                        customerInfo.Address = reader["Address"].ToString();
                        customerInfo.CustomerTypeID = Convert.ToInt32(reader["CustomerTypeID"].ToString());
                        customerInfo.DueAmount = Convert.ToDouble(reader["DueAmount"].ToString());
                        customerInfoList.Add(customerInfo);

                    }
                }

                return customerInfoList;

            }
            catch (Exception ex)
            {
                return null;
            }
}
    }
}
