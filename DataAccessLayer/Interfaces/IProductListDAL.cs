﻿using DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    public interface IProductListDAL
    {
        public Task<List<ProductListDTO>> GetAllProductList();
    }
}
