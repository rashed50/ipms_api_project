﻿namespace Models
{
    public class FactoryInfoModel
    {
        public string FactoryName { get; set; }
        public string FactoryAddress { get; set; }
        public string FactoryPhoto1 { get; set; }
        public string FactoryEmail { get; set; }
        public string FactoryMobileNo { get; set; }
        public bool FactoryIsActive { get; set; }

    }
}