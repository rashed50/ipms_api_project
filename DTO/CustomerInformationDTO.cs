﻿namespace DTO
{
    public class CustomerInformationDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string TradeName { get; set; }
        public string ContactNumber { get; set; }
        public string Address { get; set; }
        public string FatherName { get; set; }
        public string NID { get; set; }
        public int CustomerTypeID { get; set; }
        public double DueAmount { get; set; }
        public double InitialDueAmount { get; set; }
        public string Photo { get; set; }
        public string Country { get; set; }
        public string Division { get; set; }
        public string District { get; set; }
        public string Upazilla { get; set; }

        //public double TotalDueAmount { get; set; }
        //public double TotalPayAmount { get; set; }
    }
}
