﻿using DataAccessLayer.Interfaces;
using DTO;
using Foundation;
using Microsoft.Extensions.Options;
using Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class BrandDAL : BaseDBAccess, IBrandDAL
    {
        public BrandDAL(IOptions<DBConfiguration> config) : base(config)
        {

        }
        public async Task<List<BrandDTO>> GetAllBrand(int? categoryId)
        {
            try
            {
                SqlConnection conn = new SqlConnection(this.GetConnectionString());
                conn.Open();
                var sp = "Brand_Select_SP";
                SqlCommand cmd = new SqlCommand(sp, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@CategoryId", categoryId));
                IDataReader reader = await cmd.ExecuteReaderAsync();
                var brands = new List<BrandDTO>();

                if (reader != null)
                {
                    while (reader.Read())
                    {
                        var brand = new BrandDTO
                        {
                            BrandId = Convert.ToInt32(reader["BrandId"]),
                            BrandName = reader["BrandName"].ToString(),
                            BrandIsActive = Convert.ToBoolean(reader["BrandIsActive"]),
                            CategoryId = Convert.ToInt32(reader["CategoryId"])
                        };

                        brands.Add(brand);
                    }
                }

                return brands;
            }
            catch (Exception exp)
            {
                return null;
            }
        }

        public async Task AddBrand(BrandModel brandModel)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(this.GetConnectionString()))
                {

                    var sp = "Brand_Insert_SP";
                    SqlCommand cmd = new SqlCommand(sp, conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BrandName", brandModel.BrandName));
                    cmd.Parameters.Add(new SqlParameter("@CategoryId", brandModel.CategoryId));
                    cmd.Parameters.Add("@LastID", SqlDbType.Int);
                    cmd.Parameters["@LastID"].Direction = ParameterDirection.Output;
                    await conn.OpenAsync();
                    var reader = await cmd.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
