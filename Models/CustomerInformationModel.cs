﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class CustomerInformationModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string TradeName { get; set; }
        public string ContactNumber { get; set; }
        public string Address { get; set; }
        public string FatherName { get; set; }
        public string NID { get; set; }
        public int CustomerTypeId { get; set; }
        public double DueAmount { get; set; }
        public double InitialDueAmount { get; set; }
        public string Photo { get; set; }
        public int CountryId { get; set; }
        public int DivisionId { get; set; }
        public int DistrictId { get; set; }
        public int UpazillaId { get; set; }
        public int EntryById { get; set; }
    }
}
