﻿namespace Foundation
{
    public class Response
    {
        public string StatusCode { get; set; }
        public bool Success { get; set; }
        public object data { get; set; }
        public Response(string statusCode, bool success, object data)
        {
            this.StatusCode = statusCode;
            this.Success = success;
            this.data = data;
        }
    }
}
