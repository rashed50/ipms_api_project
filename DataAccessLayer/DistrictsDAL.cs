﻿using DataAccessLayer.Interfaces;
using DTO;
using Foundation;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class DistrictsDAL : BaseDBAccess, IDistrictsDAL
    {
        public DistrictsDAL(IOptions<DBConfiguration> config) : base(config)
        {

        }
        public async Task<List<DistrictsDTO>> GetAllDistricts()
        {
            try
            {
                SqlConnection conn = new SqlConnection(this.GetConnectionString());
                conn.Open();
                var sp = "sp_GetDistricts";
                SqlCommand cmd = new SqlCommand(sp, conn);
                IDataReader reader = await cmd.ExecuteReaderAsync();
                var districts = new List<DistrictsDTO>();

                if (reader != null)
                {
                    while (reader.Read())
                    {
                        var district = new DistrictsDTO
                        {
                            DistrictId = Convert.ToInt32(reader["DistrictId"]),
                            DistrictName = reader["DistrictName"].ToString(),
                            DivisionId = Convert.ToInt32(reader["DivisionId"])
                        };

                        districts.Add(district);
                    }
                }

                return districts;
            }
            catch (Exception exp)
            {
                return null;
            }
        }
    }
}
