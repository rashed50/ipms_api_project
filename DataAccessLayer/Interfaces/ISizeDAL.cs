﻿using DTO;
using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    public interface ISizeDAL
    {
        public Task<List<SizeDTO>> GetAllSize(int? brandId);
        public Task AddSize(SizeModel sizeModel);
    }
}
