﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class VendorInformationModel
    {
        [Required]
        public string VendorName { get; set; }
        [Required]
        public string ContactNumber { get; set; }
        public string Address { get; set; }
        public string VendorPhoto { get; set; }
        [Required]
        public DateTime OpeningDate { get; set; }
        [Required]
        public double Balance { get; set; }
        [Required]
        public int InitialBalance { get; set; }
        [Required]
        public int VendorAccountId { get; set; }
        [Required]
        public int EntryById { get; set; }

    }
}