﻿using DataAccessLayer.Interfaces;
using DTO;
using Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services
{
    public class ProductListService : IProductListService
    {
        private IProductListDAL _productListDAL;
        public ProductListService(IProductListDAL productListDAL)
        {
            _productListDAL = productListDAL;
        }

        public async Task<List<ProductListDTO>> GetAllProductList()
        {
            return await _productListDAL.GetAllProductList();
        }
    }
}
