﻿using DataAccessLayer.Interfaces;
using DTO;
using Services.Interfaces;

namespace Services
{
    public class CompanyInformationService : ICompanyInformationService
    {
        private ICompanyInformationDAL _companyInformationDAL;
        public CompanyInformationService(ICompanyInformationDAL companyInformationDAL)
        {
            _companyInformationDAL = companyInformationDAL;
        }
        public CompanyInformationDTO GetCompanyProfileInformation()
        {
            return _companyInformationDAL.GetCompanyProfileInformation();
        }
    }
}
