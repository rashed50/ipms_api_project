﻿using System;

namespace DTO
{
    public class EmployeeInformationDTO
    {
        public int EmployeId { get; set; }
        public string EmployeeName { get; set; }
        public string ContactNumber { get; set; }
        public string FatherName { get; set; }
        public string EmpMobileNo { get; set; }
        public string NID { get; set; }
        public string Photo { get; set; }
        public DateTime JoinDate { get; set; }
        public DateTime ReviewDate { get; set; }
        public int CountryId { get; set; }
        public int DivisionId { get; set; }
        public int DistrictId { get; set; }
        public int UpazilaId { get; set; }
        public int FactoryId { get; set; }
        public int FactoryUnitId { get; set; }
        public int DesignationId { get; set; }
        public int EmployeeTypeId { get; set; }
        public int SalaryGradeId { get; set; }
        public int PresentSalary { get; set; }
        public int StartingSalary { get; set; }
    }
}


