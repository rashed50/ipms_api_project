﻿using Microsoft.AspNetCore.Mvc;
using Models;
using Services.Interfaces;
using System;
using System.Threading.Tasks;

namespace IPMSAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StockController : ControllerBase
    {
        private IStockService _stockService;
        public StockController(IStockService stockService)
        {
            _stockService = stockService;
        }

        [HttpGet]
        // GET: StockController
        public async Task<IActionResult> Get(int? stockId)
        {
            var stocks = await _stockService.GetAllStock(stockId);

            if (stocks != null)
            {
                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = stocks
                };

                return Ok(result);
            }
            else
            {
                var result = new
                {
                    statusCode = 400,
                    success = false,
                    data = "Data Not Found",
                };

                return BadRequest(result);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] StockModel stockModel)
        {
            try
            {
                await _stockService.AddStock(stockModel);

                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = ""
                };

                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new
                {
                    statusCode = 400,
                    success = false
                };

                return BadRequest(result);
            }
        }

        [HttpGet("getbycategorybrandsizeweightid")]
        // GET: StockController
        public async Task<IActionResult> Get(int categoryId, int brandId, int sizeId, int weightId)
        {
            var stocks = await _stockService.GetAllStockByCategoryBrandSizeWeightId(categoryId, brandId, sizeId, weightId);

            if (stocks != null)
            {
                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = stocks
                };

                return Ok(result);
            }
            else
            {
                var result = new
                {
                    statusCode = 400,
                    success = false,
                    data = "Data Not Found",
                };

                return BadRequest(result);
            }
        }
    }
}
