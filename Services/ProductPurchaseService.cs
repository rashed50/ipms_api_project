﻿using DataAccessLayer;
using DataAccessLayer.Interfaces;
using DTO;
using Models;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class ProductPurchaseService : IProductPurchaseService
    {
        private static IProductPurchaseDAL _productPurchaseDAL;
        public ProductPurchaseService(IProductPurchaseDAL productPurchaseDAL)
        {
            _productPurchaseDAL = productPurchaseDAL;
        }

        public async Task<int> InsertProductPurchase(ProductPurchaseModel productPurchaseModel)
        {
            try
            {
                var id = await _productPurchaseDAL.InsertProductPurchase(productPurchaseModel);

                return id;
            }
            catch
            {
                return -1;
            }
        }

        public async Task UpdateProductPurchase(ProductPurchaseModel productPurchaseModel)
        {
            await _productPurchaseDAL.UpdateProductPurchase(productPurchaseModel);
        }

        public async Task DeleteProductPurchase(int ID)
        {
            await _productPurchaseDAL.DeleteProductPurchase(ID);
        }

        public async Task<DataTable> GetProductPurchase()
        {
            return await _productPurchaseDAL.GetProductPurchase();
        }

        public async Task<ProductPurchaseDTO> GetProductPurchaseByID(int ID)
        {
            return await _productPurchaseDAL.GetProductPurchaseByID(ID);
        }

        public async Task<DataTable> GetPurchaseReportByVendorDetailByToAndFromDate(DateTime fromdate, DateTime todate)
        {
            return await _productPurchaseDAL.GetPurchaseReportByVendorDetailByToAndFromDate(fromdate, todate);
        }

        public async Task<DataTable> GetPurchaseTransactionSummary(int vendorId, DateTime fromdate, DateTime todate)
        {
            return await _productPurchaseDAL.GetPurchaseTransactionSummary(vendorId, fromdate, todate);
        }

        public async Task<DataTable> GetPurchaseReportSummary(int vendorId, DateTime fromdate, DateTime todate)
        {
            return await _productPurchaseDAL.GetPurchaseReportSummary(vendorId, fromdate, todate);
        }

        public async Task<DataTable> GetPurchaseSummaryByVendorId(int vendorId)
        {
            return await _productPurchaseDAL.GetPurchaseSummaryByVendorId(vendorId);
        }

        public async Task<DataTable> GetPurchaseByItemSummary(DateTime fromdate, DateTime todate, int categoryId, int brandId)
        {
            return await _productPurchaseDAL.GetPurchaseByItemSummary(fromdate, todate, categoryId, brandId);
        }
    }
}
