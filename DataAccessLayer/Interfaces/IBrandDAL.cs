﻿using DTO;
using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    public interface IBrandDAL
    {
        public Task<List<BrandDTO>> GetAllBrand(int? categoryId);
        public Task AddBrand(BrandModel brandModel);
    }
}
