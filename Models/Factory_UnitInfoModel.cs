﻿namespace Models
{
    public class Factory_UnitInfoModel
    {
        public string FactoryUnitName { get; set; }
        public bool FactoryUnitIsActive { get; set; }
        public int FactoryId { get; set; }
    }
}
