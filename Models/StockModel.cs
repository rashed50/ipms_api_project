﻿namespace Models
{
    public class StockModel
    {
        public int CategoryId { get; set; }
        public int BrandId { get; set; }
        public int SizeId { get; set; }
        public int WeightId { get; set; }
        public float StockQuantity { get; set; }
    }
}
