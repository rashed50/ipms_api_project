﻿namespace DTO
{
    public class UpazilaDTO
    {
        public int UpazilaId { get; set; }
        public string UpazilaName { get; set; }
        public int DistrictId { get; set; }
    }
}
