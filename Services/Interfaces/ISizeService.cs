﻿using DTO;
using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface ISizeService
    {
        public Task<List<SizeDTO>> GetAllSize(int? brandId);
        public Task AddSize(SizeModel sizeModel);
    }
}
