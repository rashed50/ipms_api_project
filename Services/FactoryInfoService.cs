﻿using DataAccessLayer.Interfaces;
using DTO;
using Models;
using Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services
{
    public class FactoryInfoService : IFactoryInfoService
    {
        private IFactoryInfoDAL _factoryInfoDAL;
        public FactoryInfoService(IFactoryInfoDAL factoryInfoDAL)
        {
            _factoryInfoDAL = factoryInfoDAL;
        }
        public async Task<List<FactoryInfoDTO>> GetAllFactoryInfo()
        {
            return await _factoryInfoDAL.GetAllFactoryInfo();
        }

        public async Task AddFactoryInfo(FactoryInfoModel factoryInfoModel)
        {
            await _factoryInfoDAL.AddFactoryInfo(factoryInfoModel);
        }

        public async Task<FactoryInfoDTO> GetFactoryInfoById(int id)
        {
            var factoryInfo = await _factoryInfoDAL.GetFactoryInfoById(id);

            return factoryInfo;
        }
    }
}
