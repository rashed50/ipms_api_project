﻿using DataAccessLayer.Interfaces;
using DTO;
using Models;
using Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services
{
    public class StockService : IStockService
    {
        private IStockDAL _stockDAL;
        public StockService(IStockDAL stockDAL)
        {
            _stockDAL = stockDAL;
        }

        public async Task<List<StockDTO>> GetAllStock(int? stockId)
        {
            return await _stockDAL.GetAllStock(stockId);
        }

        public async Task AddStock(StockModel stockModel)
        {
            await _stockDAL.AddStock(stockModel);
        }

        public async Task<StockDTO> GetAllStockByCategoryBrandSizeWeightId(int categoryId, int brandId, int sizeId, int weightId)
        {
            return await _stockDAL.GetAllStockByCategoryBrandSizeWeightId(categoryId, brandId, sizeId, weightId);
        }
    }
}
