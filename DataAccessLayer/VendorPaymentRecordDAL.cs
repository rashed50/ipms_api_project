﻿using DataAccessLayer.Interfaces;
using DTO;
using Foundation;
using Microsoft.Extensions.Options;
using Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class VendorPaymentRecordDAL : BaseDBAccess, IVendorPaymentRecordDAL
    {
        public VendorPaymentRecordDAL(IOptions<DBConfiguration> config) : base(config)
        {

        }

        public async Task<List<VendorPaymentRecordDTO>> GetAllVendorPaymentRecord(int? vendorPaymentRecordID)
        {
            try
            {
                SqlConnection conn = new SqlConnection(this.GetConnectionString());
                conn.Open();
                var sp = "VendorPaymentRecord_SELECT_SP";
                SqlCommand cmd = new SqlCommand(sp, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                if (vendorPaymentRecordID != null)
                    cmd.Parameters.Add(new SqlParameter("@ID", vendorPaymentRecordID));
                IDataReader reader = await cmd.ExecuteReaderAsync();
                var vendorPaymentRecords = new List<VendorPaymentRecordDTO>();

                if (reader != null)
                {                   
                    while (reader.Read())
                    {
                        Console.WriteLine(reader["ID"].ToString());
                        var vendorPaymentRecord = new VendorPaymentRecordDTO
                        {
                            ID = Convert.ToInt32(reader["ID"]),
                            TransactionID = Convert.ToInt32(reader["TransactionID"]),
                            VendorID = Convert.ToInt32(reader["VendorID"]),
                            Date = DateTime.Parse(reader["Date"].ToString()).Date,
                            Amount = float.Parse(reader["Amount"].ToString()),
                            EntryByID = Convert.ToInt32(reader["EntryByID"]),
                            DebitToId = Convert.ToInt32(reader["DebitToId"]),
                            CreditToId = Convert.ToInt32(reader["CreditToId"]),
                            CheckNo = reader["CheckNo"].ToString(),
                            BankId = Convert.ToInt32(reader["BankId"]),
                            PaymentTypeId = Convert.ToInt32(reader["PaymentTypeId"]),
                        };

                        vendorPaymentRecords.Add(vendorPaymentRecord);
                    }
                }

                conn.Close();
                return vendorPaymentRecords;
            }
            catch (Exception exp)
            {
                return null;
            }
        }

        public async Task InsertVendorPaymentRecord(VendorPaymentRecordModel vendorPaymentRecordModel)
        {
            try
            {
                SqlConnection conn = new SqlConnection(this.GetConnectionString());
                SqlCommand cmd = new SqlCommand("Insert_VendorPaymentRecord_SP", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@TransactionID", vendorPaymentRecordModel.TransactionID));
                cmd.Parameters.Add(new SqlParameter("@EntryByID", vendorPaymentRecordModel.EntryByID));
                cmd.Parameters.Add(new SqlParameter("@VendorID", vendorPaymentRecordModel.VendorID));
                cmd.Parameters.Add(new SqlParameter("@Date", vendorPaymentRecordModel.Date.Date.ToString()));
                cmd.Parameters.Add(new SqlParameter("@Amount", vendorPaymentRecordModel.Amount));
                cmd.Parameters.Add(new SqlParameter("@DebitToId", vendorPaymentRecordModel.DebitToId));
                cmd.Parameters.Add(new SqlParameter("@CreditToId", vendorPaymentRecordModel.CreditToId));
                cmd.Parameters.Add(new SqlParameter("@CheckNo", vendorPaymentRecordModel.CheckNo));
                cmd.Parameters.Add(new SqlParameter("@BankId", vendorPaymentRecordModel.BankId));
                cmd.Parameters.Add(new SqlParameter("@PaymentTypeId", vendorPaymentRecordModel.PaymentTypeId));


                await conn.OpenAsync();
                await cmd.ExecuteNonQueryAsync();
                conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task DeleteVendorPaymentRecordByID(int id)
        {
            try
            {

                SqlConnection conn = new SqlConnection(this.GetConnectionString());
                SqlCommand cmd = new SqlCommand("VendorPaymentRecord_Delete_SP", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ID", id));

                await conn.OpenAsync();
                await cmd.ExecuteNonQueryAsync();
                conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
