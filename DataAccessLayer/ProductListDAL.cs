﻿using DataAccessLayer.Interfaces;
using DTO;
using Foundation;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class ProductListDAL : BaseDBAccess, IProductListDAL
    {
        public ProductListDAL(IOptions<DBConfiguration> config) : base(config)
        {

        }
        public async Task<List<ProductListDTO>> GetAllProductList()
        {
            try
            {
                SqlConnection conn = new SqlConnection(this.GetConnectionString());
                conn.Open();
                var sp = "ProductList_Select_SP";
                SqlCommand cmd = new SqlCommand(sp, conn);
                IDataReader reader = await cmd.ExecuteReaderAsync();
                var productLists = new List<ProductListDTO>();

                if (reader != null)
                {
                    while (reader.Read())
                    {
                        var productList = new ProductListDTO
                        {
                            ProductListId = Convert.ToInt32(reader["ProductListId"]),
                            CategoryName = reader["CategoryName"].ToString(),
                            BrandName = reader["BrandName"].ToString(),
                            SizeName = reader["SizeName"].ToString(),
                            WeightName = reader["WeightName"].ToString(),
                            tempPRate = Convert.ToDouble(reader["tempPRate"]),
                            IsProductActive = Convert.ToBoolean(reader["IsProductActive"])
                        };

                        productLists.Add(productList);
                    }
                }

                return productLists;
            }
            catch (Exception exp)
            {
                return null;
            }
        }
    }
}
