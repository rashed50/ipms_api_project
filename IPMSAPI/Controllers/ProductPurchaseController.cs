﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models;
using Newtonsoft.Json;
using Services;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPMSAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductPurchaseController : ControllerBase
    {
        private IProductPurchaseService _productPurchaseService;
        public ProductPurchaseController(IProductPurchaseService productPurchaseService)
        {
            _productPurchaseService = productPurchaseService;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ProductPurchaseModel productPurchaseModel)
        {
            var id = await _productPurchaseService.InsertProductPurchase(productPurchaseModel);

            if (id == -1)
            {
                var result = new
                {
                    statusCode = 400,
                    success = false
                };

                return BadRequest(result);
            }
            else
            {
                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = id
                };

                return Ok(result);
            }
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody] ProductPurchaseModel productPurchaseModel)
        {
            try
            {
                await _productPurchaseService.UpdateProductPurchase(productPurchaseModel);

                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = ""
                };

                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new
                {
                    statusCode = 400,
                    success = false
                };

                return BadRequest(result);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _productPurchaseService.DeleteProductPurchase(id);

                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = ""
                };

                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new
                {
                    statusCode = 400,
                    success = false
                };

                return BadRequest(result);
            }
        }

        [HttpGet]
        // GET: ProductPurchaseController
        public async Task<IActionResult> Get()
        {
            try
            {
                var productPurchaseList = await _productPurchaseService.GetProductPurchase();
                var list = JsonConvert.SerializeObject(productPurchaseList);

                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = list
                };

                return Ok(result);
            }
            catch
            {
                var result = new
                {
                    statusCode = 400,
                    success = false,
                    data = "Data Not Found",
                };

                return BadRequest(result);
            }
        }

        [HttpGet("GetProductPurchaseByID/{id}")]
        // GET: ProductPurchaseController
        public async Task<IActionResult> GetProductPurchaseByID(int id)
        {
            try
            {
                var productPurchase = await _productPurchaseService.GetProductPurchaseByID(id);
                var item = JsonConvert.SerializeObject(productPurchase);

                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = item
                };

                return Ok(result);
            }
            catch
            {
                var result = new
                {
                    statusCode = 400,
                    success = false,
                    data = "Data Not Found",
                };

                return BadRequest(result);
            }
        }

        [HttpGet("GetPurchaseReportByVendorDetailByToAndFromDate")]
        // GET: ProductPurchaseController
        public async Task<IActionResult> GetPurchaseReportByVendorDetailByToAndFromDate([FromQuery(Name = "fromdate")] DateTime fromdate, 
            [FromQuery(Name = "todate")] DateTime todate)
        {
            try
            {
                var productPurchaseList = await _productPurchaseService.GetPurchaseReportByVendorDetailByToAndFromDate(fromdate, todate);
                var list = JsonConvert.SerializeObject(productPurchaseList);

                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = list
                };

                return Ok(result);
            }
            catch
            {
                var result = new
                {
                    statusCode = 400,
                    success = false,
                    data = "Data Not Found",
                };

                return BadRequest(result);
            }
        }

        [HttpGet("GetPurchaseTransactionSummary")]
        public async Task<IActionResult> GetPurchaseTransactionSummary([FromQuery(Name = "vendorId")] int vendorId, [FromQuery(Name = "fromdate")] DateTime fromdate, [FromQuery(Name = "todate")] DateTime todate)
        {
            try
            {
                var productPurchaseList = await _productPurchaseService.GetPurchaseTransactionSummary(vendorId, fromdate, todate);
                var list = JsonConvert.SerializeObject(productPurchaseList);

                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = list
                };

                return Ok(result);
            }
            catch
            {
                var result = new
                {
                    statusCode = 400,
                    success = false,
                    data = "Data Not Found",
                };

                return BadRequest(result);
            }
        }

        [HttpGet("GetPurchaseReportSummary")]
        public async Task<IActionResult> GetPurchaseReportSummary([FromQuery(Name = "vendorId")] int vendorId, [FromQuery(Name = "fromdate")] DateTime fromdate, [FromQuery(Name = "todate")] DateTime todate)
        {
            try
            {
                var productPurchaseList = await _productPurchaseService.GetPurchaseReportSummary(vendorId, fromdate, todate);
                var list = JsonConvert.SerializeObject(productPurchaseList);

                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = list
                };

                return Ok(result);
            }
            catch
            {
                var result = new
                {
                    statusCode = 400,
                    success = false,
                    data = "Data Not Found",
                };

                return BadRequest(result);
            }
        }

        [HttpGet("GetPurchaseSummaryByVendorId/{vendorId}")]
        public async Task<IActionResult> GetPurchaseSummaryByVendorId(int vendorId)
        {
            try
            {
                var productPurchaseList = await _productPurchaseService.GetPurchaseSummaryByVendorId(vendorId);
                var list = JsonConvert.SerializeObject(productPurchaseList);

                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = list
                };

                return Ok(result);
            }
            catch
            {
                var result = new
                {
                    statusCode = 400,
                    success = false,
                    data = "Data Not Found",
                };

                return BadRequest(result);
            }
        }

        [HttpGet("GetPurchaseByItemSummary")]
        public async Task<IActionResult> GetPurchaseByItemSummary([FromQuery(Name = "fromdate")] DateTime fromdate, [FromQuery(Name = "todate")] DateTime todate,
            [FromQuery(Name = "categoryId")] int categoryId, [FromQuery(Name = "brandId")] int brandId)
        {
            try
            {
                var productPurchaseList = await _productPurchaseService.GetPurchaseByItemSummary(fromdate, todate, categoryId, brandId);
                var list = JsonConvert.SerializeObject(productPurchaseList);

                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = list
                };

                return Ok(result);
            }
            catch
            {
                var result = new
                {
                    statusCode = 400,
                    success = false,
                    data = "Data Not Found",
                };

                return BadRequest(result);
            }
        }
    }
}
