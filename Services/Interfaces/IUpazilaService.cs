﻿using DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IUpazilaService
    {
        public Task<List<UpazilaDTO>> GetAllUpazila();
    }
}
