﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPMSAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendorPaymentRecordController : ControllerBase
    {
        private IVendorPaymentRecordService _vendorPaymentRecordService;
        public VendorPaymentRecordController(IVendorPaymentRecordService vendorPaymentRecordService)
        {
            _vendorPaymentRecordService = vendorPaymentRecordService;
        }

        [HttpGet]
        public async Task<IActionResult> Get(int? ID)
        {
            var vendors = await _vendorPaymentRecordService.GetVendorPaymentRecord(ID);

            if (vendors != null)
            {
                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = vendors
                };

                return Ok(result);
            }
            else
            {
                var result = new
                {
                    statusCode = 400,
                    success = false,
                    data = "Data Not Found",
                };

                return BadRequest(result);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] VendorPaymentRecordModel vendorPaymentRecordModel)
        {
            try
            {
                await _vendorPaymentRecordService.AddVendorPaymentRecord(vendorPaymentRecordModel);

                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = ""
                };

                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new
                {
                    statusCode = 400,
                    success = false
                };

                return BadRequest(result);
            }
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _vendorPaymentRecordService.DeleteVendorPaymentRecordById(id);

                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = ""
                };

                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new
                {
                    statusCode = 400,
                    success = false
                };

                return BadRequest(result);
            }
        }
    }
}
