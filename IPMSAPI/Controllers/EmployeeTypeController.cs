﻿using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using System.Threading.Tasks;

namespace IPMSAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeTypeController : ControllerBase
    {
        private IEmployeeTypeService _employeeTypeService;
        public EmployeeTypeController(IEmployeeTypeService employeeTypeService)
        {
            _employeeTypeService = employeeTypeService;
        }

        [HttpGet]
        // GET: EmployeeTypeController
        public async Task<IActionResult> Get()
        {
            var employeeTypes = await _employeeTypeService.GetAllEmployeeType();

            if (employeeTypes != null)
            {
                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = employeeTypes
                };

                return Ok(result);
            }
            else
            {
                var result = new
                {
                    statusCode = 400,
                    success = false,
                    data = "Data Not Found",
                };

                return BadRequest(result);
            }
        }

        [HttpGet("getemployeetypebyid")]
        // GET: EmployeeTypeController
        public async Task<IActionResult> Get([FromQuery] int id)
        {
            var employeeTypeInfo = await _employeeTypeService.GetEmployeeTypeById(id);

            if (employeeTypeInfo != null)
            {
                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = employeeTypeInfo
                };

                return Ok(result);
            }
            else
            {
                var result = new
                {
                    statusCode = 400,
                    success = false,
                    data = "Data Not Found",
                };

                return BadRequest(result);
            }
        }
    }
}
