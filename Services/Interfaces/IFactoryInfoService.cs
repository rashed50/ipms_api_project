﻿using DTO;
using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IFactoryInfoService
    {
        public Task<List<FactoryInfoDTO>> GetAllFactoryInfo();
        public Task AddFactoryInfo(FactoryInfoModel factoryInfoModel);
        public Task<FactoryInfoDTO> GetFactoryInfoById(int id);
    }
}
