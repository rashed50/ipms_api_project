﻿namespace DTO
{
    public class Factory_UnitInfoDTO
    {
        public int FactoryUnitId { get; set; }
        public string FactoryUnitName { get; set; }
        public bool FactoryUnitIsActive { get; set; }
        public int FactoryId { get; set; }
    }
}
