﻿namespace Models
{
    public class WeightModel
    {
        public string WeightName { get; set; }
        public bool WeightIsActive { get; set; }
        public int SizeId { get; set; }

    }
}

