﻿namespace DTO
{
    public class DivisionsDTO
    {
        public int DivisionId { get; set; }
        public string DivisionName { get; set; }
        public int CountryId { get; set; }
    }
}
