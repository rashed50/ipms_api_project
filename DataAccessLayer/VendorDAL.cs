﻿using DataAccessLayer.Interfaces;
using DTO;
using Foundation;
using Microsoft.Extensions.Options;
using Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class VendorDAL : BaseDBAccess, IVendorDAL
    {
        public VendorDAL(IOptions<DBConfiguration> config) : base(config)
        {

        }
        public async Task AddVendor(VendorInformationModel vendorModel)
        {
            using SqlConnection conn = new SqlConnection(this.GetConnectionString());
            conn.Open();
            var sp = "VendorInformation_Insert_SP";
            SqlCommand cmd = new SqlCommand(sp, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters
                .Add(new SqlParameter($"@{nameof(VendorInformationModel.VendorName)}",
                vendorModel.VendorName));
            cmd.Parameters
                .Add(new SqlParameter($"@{nameof(VendorInformationModel.ContactNumber)}",
                vendorModel.ContactNumber));
            cmd.Parameters
                .Add(new SqlParameter($"@{nameof(VendorInformationModel.Address)}",
                vendorModel.Address));
            cmd.Parameters
                .Add(new SqlParameter($"@{nameof(VendorInformationModel.Balance)}",
                vendorModel.Balance));
            cmd.Parameters
                .Add(new SqlParameter($"@{nameof(VendorInformationModel.InitialBalance)}",
                vendorModel.InitialBalance));
            cmd.Parameters
                .Add(new SqlParameter($"@{nameof(VendorInformationModel.OpeningDate)}",
                vendorModel.OpeningDate));
            cmd.Parameters
                .Add(new SqlParameter($"@{nameof(VendorInformationModel.EntryById)}",
                vendorModel.EntryById));
            cmd.Parameters
                .Add(new SqlParameter($"@{nameof(VendorInformationModel.VendorAccountId)}",
                vendorModel.VendorAccountId));
            cmd.Parameters
                .Add(new SqlParameter($"@{nameof(VendorInformationModel.VendorPhoto)}",
                vendorModel.VendorPhoto));


            await cmd.ExecuteNonQueryAsync();
        }

        public async Task<List<VendorInformationDTO>> GetAllVendor()
        {
            SqlConnection conn = new SqlConnection(this.GetConnectionString());
            conn.Open();
            var sp = "VendorInformation_Select_SP";
            SqlCommand cmd = new SqlCommand(sp, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@VendorId", null));
            IDataReader reader = await cmd.ExecuteReaderAsync();
            var vendors = new List<VendorInformationDTO>();

            if (reader != null)
            {
                while (reader.Read())
                {
                    var vendor = new VendorInformationDTO
                    {
                        VendorId = Convert.ToInt32(reader[nameof(VendorInformationDTO.VendorId)]),
                        VendorName = reader[nameof(VendorInformationDTO.VendorName)].ToString(),
                        ContactNumber = reader[nameof(VendorInformationDTO.ContactNumber)].ToString(),
                        Address = reader[nameof(VendorInformationDTO.Address)].ToString(),
                        VendorAccountId = Convert.ToInt32(reader[nameof(VendorInformationDTO.VendorAccountId)]),
                        Balance = Convert.ToDouble(reader[nameof(VendorInformationDTO.Balance)]),
                        InitialBalance = Convert.ToInt32(reader[nameof(VendorInformationDTO.InitialBalance)]),
                        VendorPhoto = reader[nameof(VendorInformationDTO.VendorPhoto)].ToString(),
                        EntryById = Convert.ToInt32(reader[nameof(VendorInformationDTO.EntryById)]),
                        UserName = reader[nameof(VendorInformationDTO.UserName)].ToString(),
                        OpeningDate = Convert.ToDateTime(reader[nameof(VendorInformationDTO.OpeningDate)])
                    };

                    vendors.Add(vendor);
                }
            }

            return vendors;
        }

        public Task<VendorInformationDTO> GetVendorById(int categoryId)
        {
            throw new NotImplementedException();
        }
    }
}


//USE[starhair_ipmsdb]
//GO
///****** Object:  StoredProcedure [dbo].[Brand_Select_SP]    Script Date: 7/11/2021 6:47:45 PM ******/
//SET ANSI_NULLS ON
//GO
//SET QUOTED_IDENTIFIER ON
//GO
//CREATE PROCEDURE [dbo].[VendorInformation_Insert_SP]
//@VendorId INT = NULL
//	@ContactNumber VARCHAR(255),
//	@Address VARCHAR(255),
//	@VendorPhoto VARCHAR(255),
//	@OpeningDate DateTime,
//    @Balance DOUBLE,
//    @VendorAccountId INT,
//    @EntryById INT,

//AS
//BEGIN
//    SET NOCOUNT ON;

//INSERT INTO[dbo].[VendorInformation]
//(

//           [VendorName]
//          ,[ContactNumber]
//          ,[Address]
//          ,[VendorPhoto]
//          ,[Balance]
//          ,[InitialBalance]
//          ,[OpeningDate]
//          ,[VendorAccoutId]
//          ,[EntryById]
//          );
//VALUES
//       (
//       @EmployeeName,
//       @ContactNumber,
//       @FatherName,
//       @EmpMobileNo,
//       @NID,
//       @Photo,
//       @JoinDate,
//       @ReviewDate,
//       @CountryId,
//       @DivisionId,
//       @DistrictId,
//       @UpazilaId,
//       @FactoryId,
//       @FactoryUnitId,
//       @DesignationId,
//       @EmployeeTypeId,
//       @SalaryGradeId,
//       @PresentSalary,
//       @StartingSalary
//       );

//END

