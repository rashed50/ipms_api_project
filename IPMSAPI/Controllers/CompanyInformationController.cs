﻿using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace IPMSAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyInformationController : ControllerBase
    {
        // GET: api/<CompanyInformationController>

        private ICompanyInformationService _companyInformationService;
        public CompanyInformationController(ICompanyInformationService companyInformationService)
        {
            _companyInformationService = companyInformationService;
        }

        [HttpGet]
        public IActionResult Get()
        {

            var comProfileDTO = _companyInformationService.GetCompanyProfileInformation();

            if (comProfileDTO != null)
            {
                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = comProfileDTO
                };

                return Ok(result);
            }
            else
            {
                var result = new
                {
                    statusCode = 200,
                    success = false,
                    data = "Data Not Found",
                };

                return Ok(result);
            }

        }





















        // GET api/<CompanyInformationController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<CompanyInformationController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<CompanyInformationController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<CompanyInformationController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
