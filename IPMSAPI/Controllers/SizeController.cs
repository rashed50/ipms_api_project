﻿using Microsoft.AspNetCore.Mvc;
using Models;
using Services.Interfaces;
using System;
using System.Threading.Tasks;

namespace IPMSAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SizeController : ControllerBase
    {
        private ISizeService _sizeService;
        public SizeController(ISizeService sizeService)
        {
            _sizeService = sizeService;
        }

        [HttpGet]
        // GET: SizeController
        public async Task<IActionResult> Get(int? brandId)
        {
            var sizes = await _sizeService.GetAllSize(brandId);

            if (sizes != null)
            {
                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = sizes
                };

                return Ok(result);
            }
            else
            {
                var result = new
                {
                    statusCode = 400,
                    success = false,
                    data = "Data Not Found",
                };

                return BadRequest(result);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] SizeModel sizeModel)
        {
            try
            {
                await _sizeService.AddSize(sizeModel);

                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = ""
                };

                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new
                {
                    statusCode = 400,
                    success = false
                };

                return BadRequest(result);
            }
        }
    }
}
