﻿namespace DTO
{
    public class BrandDTO
    {
        public int BrandId { get; set; }
        public string BrandName { get; set; }
        public bool BrandIsActive { get; set; }
        public int CategoryId { get; set; }
    }
}
