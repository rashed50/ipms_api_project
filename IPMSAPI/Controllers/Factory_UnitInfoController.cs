﻿using Microsoft.AspNetCore.Mvc;
using Models;
using Services.Interfaces;
using System;
using System.Threading.Tasks;

namespace IPMSAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Factory_UnitInfoController : ControllerBase
    {
        private IFactory_UnitInfoService _factory_UnitInfoService;
        public Factory_UnitInfoController(IFactory_UnitInfoService factory_UnitInfoService)
        {
            _factory_UnitInfoService = factory_UnitInfoService;
        }

        [HttpGet]
        // GET: Factory_UnitInfoController
        public async Task<IActionResult> Get()
        {
            var factory_UnitInfos = await _factory_UnitInfoService.GetAllFactory_UnitInfo();
            if (factory_UnitInfos != null)
            {
                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = factory_UnitInfos
                };

                return Ok(result);
            }
            else
            {
                var result = new
                {
                    statusCode = 400,
                    success = false,
                    data = "Data Not Found",
                };

                return BadRequest(result);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Factory_UnitInfoModel factory_UnitInfoModel)
        {
            try
            {
                await _factory_UnitInfoService.AddFactory_UnitInfo(factory_UnitInfoModel);

                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = ""
                };

                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new
                {
                    statusCode = 400,
                    success = false
                };

                return BadRequest(result);
            }
        }
    }
}
