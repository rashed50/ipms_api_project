﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class PurchaseProductRecordListModel
    {
        public int ID { get; set; }
        public int ProductListID { get; set; }
        public double Quantity { get; set; }
        public double UnitPrice { get; set; }
        public int ProductPurchaseID { get; set; }
        public double Amount { get; set; }
    }
}
