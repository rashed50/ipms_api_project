﻿using DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    public interface IUpazilaDAL
    {
        public Task<List<UpazilaDTO>> GetAllUpazila();
    }
}
