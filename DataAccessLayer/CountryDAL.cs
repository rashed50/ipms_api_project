﻿using DataAccessLayer.Interfaces;
using DTO;
using Foundation;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class CountryDAL : BaseDBAccess, ICountryDAL
    {
        public CountryDAL(IOptions<DBConfiguration> config) : base(config)
        {

        }
        public async Task<List<CountryDTO>> GetAllCountry()
        {
            try
            {
                SqlConnection conn = new SqlConnection(this.GetConnectionString());
                conn.Open();
                var sp = "sp_GetCountry";
                SqlCommand cmd = new SqlCommand(sp, conn);
                IDataReader reader = await cmd.ExecuteReaderAsync();
                var countries = new List<CountryDTO>();

                if (reader != null)
                {
                    while (reader.Read())
                    {
                        var country = new CountryDTO
                        {
                            CountryId = Convert.ToInt32(reader["CountryId"]),
                            CountryName = reader["CountryName"].ToString(),
                        };

                        countries.Add(country);
                    }
                }

                return countries;
            }
            catch (Exception exp)
            {
                return null;
            }
        }
    }
}
