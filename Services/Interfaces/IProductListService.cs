﻿using DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IProductListService
    {
        public Task<List<ProductListDTO>> GetAllProductList();
    }
}
