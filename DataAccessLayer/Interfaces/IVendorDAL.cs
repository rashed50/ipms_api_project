﻿using DTO;
using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    public interface IVendorDAL
    {
        public Task<VendorInformationDTO> GetVendorById(int categoryId);
        public Task<List<VendorInformationDTO>> GetAllVendor();
        public Task AddVendor(VendorInformationModel brandModel);
    }
}
