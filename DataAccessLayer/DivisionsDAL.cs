﻿using DataAccessLayer.Interfaces;
using DTO;
using Foundation;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class DivisionsDAL : BaseDBAccess, IDivisionsDAL
    {
        public DivisionsDAL(IOptions<DBConfiguration> config) : base(config)
        {

        }
        public async Task<List<DivisionsDTO>> GetAllDivisions()
        {
            try
            {
                SqlConnection conn = new SqlConnection(this.GetConnectionString());
                conn.Open();
                var sp = "sp_GetDivisions";
                SqlCommand cmd = new SqlCommand(sp, conn);
                IDataReader reader = await cmd.ExecuteReaderAsync();
                var divisions = new List<DivisionsDTO>();

                if (reader != null)
                {
                    while (reader.Read())
                    {
                        var division = new DivisionsDTO
                        {
                            DivisionId = Convert.ToInt32(reader["DivisionId"]),
                            DivisionName = reader["DivisionName"].ToString(),
                            CountryId = Convert.ToInt32(reader["CountryId"])
                        };

                        divisions.Add(division);
                    }
                }

                return divisions;
            }
            catch (Exception exp)
            {
                return null;
            }
        }
    }
}
