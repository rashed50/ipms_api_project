FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /src
COPY ["IPMSAPI/IPMSAPI.csproj","IPMSAPI/"]
COPY ["DataAccessLayer/DataAccessLayer.csproj","DataAccessLayer/"]
COPY ["Models/Models.csproj","Models/"]
COPY ["Services/Services.csproj","Services/"]
COPY ["Foundation/Foundation.csproj","Foundation/"]
COPY ["DTO/DTO.csproj","DTO/"]
RUN dotnet restore "IPMSAPI/IPMSAPI.csproj"
COPY . .
WORKDIR "/src/IPMSAPI"
RUN dotnet build "IPMSAPI.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "IPMSAPI.csproj" -c Release -o /app

FROM build AS final 
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT  ["dotnet","IPMSAPI.dll"]
