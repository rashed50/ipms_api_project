﻿using DTO;
using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    public interface IWeightDAL
    {
        public Task<List<WeightDTO>> GetAllWeight(int? sizeId);
        public Task AddWeight(WeightModel weightModel);
    }
}
