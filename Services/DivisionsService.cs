﻿using DataAccessLayer.Interfaces;
using DTO;
using Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services
{
    public class DivisionsService : IDivisionsService
    {
        private IDivisionsDAL _divisionsDAL;
        public DivisionsService(IDivisionsDAL divisionsDAL)
        {
            _divisionsDAL = divisionsDAL;
        }

        public async Task<List<DivisionsDTO>> GetAllDivisions()
        {
            return await _divisionsDAL.GetAllDivisions();
        }
    }
}
