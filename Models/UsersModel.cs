﻿namespace Models
{
    class UsersModel
    {
        public string UserName { get; set; }
        public string UserPassword { get; set; }
        public int EmployeId { get; set; }
        public int RoleId { get; set; }
    }
}
