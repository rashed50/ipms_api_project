﻿using DataAccessLayer.Interfaces;
using DTO;
using Foundation;
using Microsoft.Extensions.Options;
using Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class Factory_UnitInfoDAL : BaseDBAccess, IFactory_UnitInfoDAL
    {
        public Factory_UnitInfoDAL(IOptions<DBConfiguration> config) : base(config)
        {

        }

        public async Task<List<Factory_UnitInfoDTO>> GetAllFactory_UnitInfo()
        {
            try
            {
                SqlConnection conn = new SqlConnection(this.GetConnectionString());
                conn.Open();
                SqlCommand cmd = new SqlCommand("Select * from Factory_UnitInfo", conn);
                IDataReader reader = await cmd.ExecuteReaderAsync();
                var factory_UnitInfoDTOs = new List<Factory_UnitInfoDTO>();

                if (reader != null)
                {
                    while (reader.Read())
                    {
                        var factory_UnitInfoDTO = new Factory_UnitInfoDTO
                        {
                            FactoryUnitId = Convert.ToInt32(reader["FactoryUnitId"]),
                            FactoryUnitName = reader["FactoryUnitName"].ToString(),
                            FactoryUnitIsActive = Convert.ToBoolean(reader["FactoryUnitIsActive"]),
                            FactoryId = Convert.ToInt32(reader["FactoryId"])
                        };

                        factory_UnitInfoDTOs.Add(factory_UnitInfoDTO);
                    }
                }

                return factory_UnitInfoDTOs;
            }
            catch (Exception exp)
            {
                return null;
            }
        }

        public async Task AddFactory_UnitInfo(Factory_UnitInfoModel factory_UnitInfoModel)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(this.GetConnectionString()))
                {

                    var sp = "sp_AddFactory_UnitInfo";
                    SqlCommand cmd = new SqlCommand(sp, conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@FactoryUnitName", factory_UnitInfoModel.FactoryUnitName));
                    cmd.Parameters.Add(new SqlParameter("@FactoryId", factory_UnitInfoModel.FactoryId));
                    await conn.OpenAsync();
                    var reader = await cmd.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
