﻿namespace Models
{
    class DesignationModel
    {
        public string DesignationName { get; set; }
        public string DesignationDesciption { get; set; }
        public bool DesignationIsActive { get; set; }

    }
}
