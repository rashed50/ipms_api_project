﻿using DataAccessLayer.Interfaces;
using DTO;
using Models;
using Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services
{
    public class WeightService : IWeightService
    {
        private IWeightDAL _weightDAL;
        public WeightService(IWeightDAL weightDAL)
        {
            _weightDAL = weightDAL;
        }

        public async Task<List<WeightDTO>> GetAllWeight(int? sizeId)
        {
            return await _weightDAL.GetAllWeight(sizeId);
        }

        public async Task AddWeight(WeightModel weightModel)
        {
            await _weightDAL.AddWeight(weightModel);
        }
    }
}
