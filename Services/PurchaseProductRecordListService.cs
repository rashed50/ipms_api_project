﻿using DataAccessLayer.Interfaces;
using DTO;
using Models;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class PurchaseProductRecordListService : IPurchaseProductRecordListService
    {
        private IPurchaseProductRecordListDAL _purchaseProductRecordListDAL;
        public PurchaseProductRecordListService(IPurchaseProductRecordListDAL purchaseProductRecordListDAL)
        {
            _purchaseProductRecordListDAL = purchaseProductRecordListDAL;
        }

        public async Task InsertPurchaseProductRecordList(PurchaseProductRecordListModel purchaseProductRecordListModel)
        {
            await _purchaseProductRecordListDAL.InsertPurchaseProductRecordList(purchaseProductRecordListModel);
        }

        public async Task UpdatePurchaseProductRecordList(PurchaseProductRecordListModel purchaseProductRecordListModel)
        {
            await _purchaseProductRecordListDAL.UpdatePurchaseProductRecordList(purchaseProductRecordListModel);
        }

        public async Task DeletePurchaseProductRecordList(int id)
        {
            await _purchaseProductRecordListDAL.DeletePurchaseProductRecordList(id);
        }

        public async Task<List<PurchaseProductRecordListDTO>> GetPurchaseProductRecordList()
        {
            return await _purchaseProductRecordListDAL.GetPurchaseProductRecordList();
        }

        public async Task<PurchaseProductRecordListDTO> GetPurchaseProductRecordListByID(int id)
        {
            return await _purchaseProductRecordListDAL.GetPurchaseProductRecordListByID(id);
        }

        public async Task<List<PurchaseProductRecordListDTO>> GetPurchaseProductRecordListBySellReturn(int sellReturnID)
        {
            return await _purchaseProductRecordListDAL.GetPurchaseProductRecordListBySellReturn(sellReturnID);
        }

        public async Task<DataTable> GetAllSellReturnSummaryList(int year)
        {
            return await _purchaseProductRecordListDAL.GetAllSellReturnSummaryList(year);
        }

        public async Task<DataTable> GetAllSellReturnRecordList(int sellReturnId)
        {
            return await _purchaseProductRecordListDAL.GetAllSellReturnRecordList(sellReturnId);
        }

        public async Task DeletePurchaseProductRecordListByPurchaseID(int id)
        {
            await _purchaseProductRecordListDAL.DeletePurchaseProductRecordListByPurchaseID(id);
        }
    }
}
