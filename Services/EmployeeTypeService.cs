﻿using DataAccessLayer.Interfaces;
using DTO;
using Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services
{
    public class EmployeeTypeService : IEmployeeTypeService
    {
        private IEmployeeTypeDAL _employeeTypeDAL;
        public EmployeeTypeService(IEmployeeTypeDAL employeeTypeDAL)
        {
            _employeeTypeDAL = employeeTypeDAL;
        }

        public async Task<List<EmployeeTypeDTO>> GetAllEmployeeType()
        {
            return await _employeeTypeDAL.GetAllEmployeeType();
        }

        public async Task<EmployeeTypeDTO> GetEmployeeTypeById(int id)
        {
            var employeeType = await _employeeTypeDAL.GetEmployeeTypeById(id);

            return employeeType;
        }
    }
}
