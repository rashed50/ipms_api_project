﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class PurchaseProductRecordListDTO
    {
        public string ProductCatgoryName { get; set; }
        public int ID { get; set; }
        public int CategoryId { get; set; }
        public string ProductBrandName { get; set; }
        public int BrandNameId { get; set; }
        public string ProductSizeName { get; set; }
        public int SizeNameId { get; set; }
        public string ProductThickness { get; set; }
        public int ThicknessId { get; set; }
        public double UnitPrice { get; set; }
        public double Quantity { get; set; }
        public double Amount { get; set; }
        public double LabourCost { get; set; }
        public string VendorName { get; set; }
        public int VendorID { get; set; }
        public string StaffName { get; set; }
        public DateTime PurchaseDate { get; set; }
        public int ProductPurchaseID { get; set; }
        public int TransactionID { get; set; }
        public double TotalAmount { get; set; }
        public int ProductListID { get; set; }
        public bool IsProductActive { get; set; }
    }
}
