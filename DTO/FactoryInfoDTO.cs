﻿namespace DTO
{
    public class FactoryInfoDTO
    {
        public int FactoryId { get; set; }
        public string FactoryName { get; set; }
        public string FactoryAddress { get; set; }
        public string FactoryPhoto1 { get; set; }
        public string FactoryEmail { get; set; }
        public string FactoryMobile { get; set; }
        public bool FactoryIsActive { get; set; }

    }
}
