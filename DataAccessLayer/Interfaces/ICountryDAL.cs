﻿using DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    public interface ICountryDAL
    {
        public Task<List<CountryDTO>> GetAllCountry();
    }
}
