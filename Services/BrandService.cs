﻿using DataAccessLayer.Interfaces;
using DTO;
using Models;
using Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services
{
    public class BrandService : IBrandService
    {
        private IBrandDAL _brandDAL;
        public BrandService(IBrandDAL brandDAL)
        {
            _brandDAL = brandDAL;
        }

        public async Task<List<BrandDTO>> GetAllBrand(int? categoryId)
        {
            return await _brandDAL.GetAllBrand(categoryId);
        }

        public async Task AddBrand(BrandModel brandModel)
        {
            await _brandDAL.AddBrand(brandModel);
        }
    }
}
