﻿using DTO;
using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface ICategoryService
    {
        public Task<List<CategoryDTO>> GetAllCategory();
        public Task AddCategory(CategoryModel categoryModel);
    }
}
