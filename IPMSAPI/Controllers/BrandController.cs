﻿using Microsoft.AspNetCore.Mvc;
using Models;
using Services.Interfaces;
using System;
using System.Threading.Tasks;

namespace IPMSAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BrandController : ControllerBase
    {
        private IBrandService _brandService;
        public BrandController(IBrandService brandService)
        {
            _brandService = brandService;
        }

        [HttpGet]
        // GET: BrandController
        public async Task<IActionResult> Get(int? categoryId)
        {
            var brands = await _brandService.GetAllBrand(categoryId);

            if (brands != null)
            {
                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = brands
                };

                return Ok(result);
            }
            else
            {
                var result = new
                {
                    statusCode = 400,
                    success = false,
                    data = "Data Not Found",
                };

                return BadRequest(result);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] BrandModel brandModel)
        {
            try
            {
                await _brandService.AddBrand(brandModel);

                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = ""
                };

                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new
                {
                    statusCode = 400,
                    success = false
                };

                return BadRequest(result);
            }
        }
    }
}

