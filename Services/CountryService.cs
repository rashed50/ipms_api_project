﻿using DataAccessLayer.Interfaces;
using DTO;
using Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services
{
    public class CountryService : ICountryService
    {
        private ICountryDAL _countryDAL;
        public CountryService(ICountryDAL countryDAL)
        {
            _countryDAL = countryDAL;
        }

        public async Task<List<CountryDTO>> GetAllCountry()
        {
            return await _countryDAL.GetAllCountry();
        }
    }
}
