﻿using Microsoft.AspNetCore.Mvc;
using Models;
using Services.Interfaces;
using System;
using System.Threading.Tasks;

namespace IPMSAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeInformationController : ControllerBase
    {
        private IEmployeeInformationService _employeeInformationService;
        public EmployeeInformationController(IEmployeeInformationService employeeInformationService)
        {
            _employeeInformationService = employeeInformationService;
        }

        [HttpGet]
        // GET: EmployeeInformationController
        public async Task<IActionResult> Get()
        {
            var employees = await _employeeInformationService.GetAllEmployeeInformation();

            if (employees != null)
            {
                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = employees
                };

                return Ok(result);
            }
            else
            {
                var result = new
                {
                    statusCode = 400,
                    success = false,
                    data = "Data Not Found",
                };

                return BadRequest(result);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] EmployeeInformationModel employeeInformationModel)
        {
            try
            {
                await _employeeInformationService.AddEmployeeInformation(employeeInformationModel);

                var result = new
                {
                    statusCode = 200,
                    success = true,
                    data = ""
                };

                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new
                {
                    statusCode = 400,
                    success = false
                };

                return BadRequest(result);
            }
        }
    }
}